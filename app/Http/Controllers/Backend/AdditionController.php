<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\AdditionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdditionRequest;
use App\Http\Requests\UpdateAdditionRequest;
use App\Repositories\AdditionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Traits\Authorizable;
use Response;

class AdditionController extends AppBaseController
{
    use Authorizable;
    /** @var  AdditionRepository */
    private $additionRepository;

    public function __construct(AdditionRepository $additionRepo)
    {
        $this->additionRepository = $additionRepo;
    }

    /**
     * Display a listing of the Addition.
     *
     * @param AdditionDataTable $additionDataTable
     * @return Response
     */
    public function index(AdditionDataTable $additionDataTable)
    {
        return $additionDataTable->render('backend.additions.index');
    }

    /**
     * Show the form for creating a new Addition.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.additions.create');
    }

    /**
     * Store a newly created Addition in storage.
     *
     * @param CreateAdditionRequest $request
     *
     * @return Response
     */
    public function store(CreateAdditionRequest $request)
    {
        $input = $request->all();

        $addition = $this->additionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/additions.singular')]));

        return redirect(route('backend.additions.index'));
    }

    /**
     * Display the specified Addition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            Flash::error(__('models/additions.singular').' '.__('messages.not_found'));

            return redirect(route('backend.additions.index'));
        }

        return view('backend.additions.show')->with('addition', $addition);
    }

    /**
     * Show the form for editing the specified Addition.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            Flash::error(__('messages.not_found', ['model' => __('models/additions.singular')]));

            return redirect(route('backend.additions.index'));
        }

        return view('backend.additions.edit')->with('addition', $addition);
    }

    /**
     * Update the specified Addition in storage.
     *
     * @param  int              $id
     * @param UpdateAdditionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdditionRequest $request)
    {
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            Flash::error(__('messages.not_found', ['model' => __('models/additions.singular')]));

            return redirect(route('backend.additions.index'));
        }

        $addition = $this->additionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/additions.singular')]));

        return redirect(route('backend.additions.index'));
    }

    /**
     * Remove the specified Addition from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            Flash::error(__('messages.not_found', ['model' => __('models/additions.singular')]));

            return redirect(route('backend.additions.index'));
        }

        $this->additionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/additions.singular')]));

        return redirect(route('backend.additions.index'));
    }
}
