<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\SizeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSizeRequest;
use App\Http\Requests\UpdateSizeRequest;
use App\Repositories\SizeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Traits\Authorizable;
use Response;

class SizeController extends AppBaseController
{
    use Authorizable;
    /** @var  SizeRepository */
    private $sizeRepository;

    public function __construct(SizeRepository $sizeRepo)
    {
        $this->sizeRepository = $sizeRepo;
    }

    /**
     * Display a listing of the Size.
     *
     * @param SizeDataTable $sizeDataTable
     * @return Response
     */
    public function index(SizeDataTable $sizeDataTable)
    {
        return $sizeDataTable->render('backend.sizes.index');
    }

    /**
     * Show the form for creating a new Size.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.sizes.create');
    }

    /**
     * Store a newly created Size in storage.
     *
     * @param CreateSizeRequest $request
     *
     * @return Response
     */
    public function store(CreateSizeRequest $request)
    {
        $input = $request->all();

        $size = $this->sizeRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/sizes.singular')]));

        return redirect(route('backend.sizes.index'));
    }

    /**
     * Display the specified Size.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $size = $this->sizeRepository->find($id);

        if (empty($size)) {
            Flash::error(__('models/sizes.singular').' '.__('messages.not_found'));

            return redirect(route('backend.sizes.index'));
        }

        return view('backend.sizes.show')->with('size', $size);
    }

    /**
     * Show the form for editing the specified Size.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $size = $this->sizeRepository->find($id);

        if (empty($size)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sizes.singular')]));

            return redirect(route('backend.sizes.index'));
        }

        return view('backend.sizes.edit')->with('size', $size);
    }

    /**
     * Update the specified Size in storage.
     *
     * @param  int              $id
     * @param UpdateSizeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSizeRequest $request)
    {
        $size = $this->sizeRepository->find($id);

        if (empty($size)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sizes.singular')]));

            return redirect(route('backend.sizes.index'));
        }

        $size = $this->sizeRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/sizes.singular')]));

        return redirect(route('backend.sizes.index'));
    }

    /**
     * Remove the specified Size from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $size = $this->sizeRepository->find($id);

        if (empty($size)) {
            Flash::error(__('messages.not_found', ['model' => __('models/sizes.singular')]));

            return redirect(route('backend.sizes.index'));
        }

        $this->sizeRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/sizes.singular')]));

        return redirect(route('backend.sizes.index'));
    }
}
