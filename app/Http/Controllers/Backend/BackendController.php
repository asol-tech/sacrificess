<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\AppBaseController;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;

class BackendController extends AppBaseController
{
    // use Authorizable;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersNum = User::count();
        $staffNum = User::whenRole('staff')->count();
        $itemsNum = Item::count();
        $ordersNum = Order::count();
        $orders = Order::orderBy('id', 'desc')->limit(15)->get();
        return view('backend.home', compact('orders', 'usersNum', 'staffNum', 'itemsNum', 'ordersNum'));
    }

    public function mediaTmp(Request $request)
    {
        $request->validate([
            'file' => 'required|image'
        ]);

        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    public function settings(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('backend.settings');
        }

        if ($request->isMethod('post')) {
            settings()->set($request->all());
            settings()->save();
            Flash::success(__('messages.saved', ['model' => __('site.settings')]));
            return redirect(route('backend.settings'));
        }
    }
}
