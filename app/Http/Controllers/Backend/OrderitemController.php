<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\OrderitemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrderitemRequest;
use App\Http\Requests\UpdateOrderitemRequest;
use App\Repositories\OrderitemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrderitemController extends AppBaseController
{
    /** @var  OrderitemRepository */
    private $orderitemRepository;

    public function __construct(OrderitemRepository $orderitemRepo)
    {
        $this->orderitemRepository = $orderitemRepo;
    }

    /**
     * Display a listing of the Orderitem.
     *
     * @param OrderitemDataTable $orderitemDataTable
     * @return Response
     */
    public function index(OrderitemDataTable $orderitemDataTable)
    {
        return $orderitemDataTable->render('backend.orderitems.index');
    }

    /**
     * Show the form for creating a new Orderitem.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.orderitems.create');
    }

    /**
     * Store a newly created Orderitem in storage.
     *
     * @param CreateOrderitemRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderitemRequest $request)
    {
        $input = $request->all();

        $orderitem = $this->orderitemRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/orderitems.singular')]));

        return redirect(route('backend.orderitems.index'));
    }

    /**
     * Display the specified Orderitem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            Flash::error(__('models/orderitems.singular').' '.__('messages.not_found'));

            return redirect(route('backend.orderitems.index'));
        }

        return view('backend.orderitems.show')->with('orderitem', $orderitem);
    }

    /**
     * Show the form for editing the specified Orderitem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orderitems.singular')]));

            return redirect(route('backend.orderitems.index'));
        }

        return view('backend.orderitems.edit')->with('orderitem', $orderitem);
    }

    /**
     * Update the specified Orderitem in storage.
     *
     * @param  int              $id
     * @param UpdateOrderitemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderitemRequest $request)
    {
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orderitems.singular')]));

            return redirect(route('backend.orderitems.index'));
        }

        $orderitem = $this->orderitemRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/orderitems.singular')]));

        return redirect(route('backend.orderitems.index'));
    }

    /**
     * Remove the specified Orderitem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/orderitems.singular')]));

            return redirect(route('backend.orderitems.index'));
        }

        $this->orderitemRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/orderitems.singular')]));

        return redirect(route('backend.orderitems.index'));
    }
}
