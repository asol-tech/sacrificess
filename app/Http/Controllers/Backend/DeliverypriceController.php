<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\DeliverypriceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDeliverypriceRequest;
use App\Http\Requests\UpdateDeliverypriceRequest;
use App\Repositories\DeliverypriceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Traits\Authorizable;
use Response;

class DeliverypriceController extends AppBaseController
{
    use Authorizable;
    /** @var  DeliverypriceRepository */
    private $deliverypriceRepository;

    public function __construct(DeliverypriceRepository $deliverypriceRepo)
    {
        $this->deliverypriceRepository = $deliverypriceRepo;
    }

    /**
     * Display a listing of the Deliveryprice.
     *
     * @param DeliverypriceDataTable $deliverypriceDataTable
     * @return Response
     */
    public function index(DeliverypriceDataTable $deliverypriceDataTable)
    {
        return $deliverypriceDataTable->render('backend.deliveryprices.index');
    }

    /**
     * Show the form for creating a new Deliveryprice.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.deliveryprices.create');
    }

    /**
     * Store a newly created Deliveryprice in storage.
     *
     * @param CreateDeliverypriceRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliverypriceRequest $request)
    {
        $input = $request->all();

        $deliveryprice = $this->deliverypriceRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/deliveryprices.singular')]));

        return redirect(route('backend.deliveryprices.index'));
    }

    /**
     * Display the specified Deliveryprice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            Flash::error(__('models/deliveryprices.singular').' '.__('messages.not_found'));

            return redirect(route('backend.deliveryprices.index'));
        }

        return view('backend.deliveryprices.show')->with('deliveryprice', $deliveryprice);
    }

    /**
     * Show the form for editing the specified Deliveryprice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryprices.singular')]));

            return redirect(route('backend.deliveryprices.index'));
        }

        return view('backend.deliveryprices.edit')->with('deliveryprice', $deliveryprice);
    }

    /**
     * Update the specified Deliveryprice in storage.
     *
     * @param  int              $id
     * @param UpdateDeliverypriceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliverypriceRequest $request)
    {
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryprices.singular')]));

            return redirect(route('backend.deliveryprices.index'));
        }

        $deliveryprice = $this->deliverypriceRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/deliveryprices.singular')]));

        return redirect(route('backend.deliveryprices.index'));
    }

    /**
     * Remove the specified Deliveryprice from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            Flash::error(__('messages.not_found', ['model' => __('models/deliveryprices.singular')]));

            return redirect(route('backend.deliveryprices.index'));
        }

        $this->deliverypriceRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/deliveryprices.singular')]));

        return redirect(route('backend.deliveryprices.index'));
    }
}
