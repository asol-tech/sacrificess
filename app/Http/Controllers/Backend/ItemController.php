<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Repositories\ItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Traits\Authorizable;
use Response;

class ItemController extends AppBaseController
{
    use Authorizable;
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param ItemDataTable $itemDataTable
     * @return Response
     */
    public function index(ItemDataTable $itemDataTable)
    {
        return $itemDataTable->render('backend.items.index');
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.items.create');
    }

    /**
     * Store a newly created Item in storage.
     *
     * @param CreateItemRequest $request
     *
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        $input = $request->all();

        $item = $this->itemRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/items.singular')]));

        return redirect(route('backend.items.index'));
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error(__('models/items.singular').' '.__('messages.not_found'));

            return redirect(route('backend.items.index'));
        }

        return view('backend.items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error(__('messages.not_found', ['model' => __('models/items.singular')]));

            return redirect(route('backend.items.index'));
        }

        return view('backend.items.edit')->with('item', $item);
    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int              $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error(__('messages.not_found', ['model' => __('models/items.singular')]));

            return redirect(route('backend.items.index'));
        }

        $item = $this->itemRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/items.singular')]));

        return redirect(route('backend.items.index'));
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Flash::error(__('messages.not_found', ['model' => __('models/items.singular')]));

            return redirect(route('backend.items.index'));
        }

        $this->itemRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/items.singular')]));

        return redirect(route('backend.items.index'));
    }
}
