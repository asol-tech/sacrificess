<?php

namespace App\Http\Controllers\API\Auth;

use App\Exceptions\MyApiException;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\ResetUserAPIRequest;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User;
use App\PasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Twilio\Rest\Client;

class PasswordResetController extends AppBaseController
{
    use SendsPasswordResetEmails;

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
        /**
     * @param Request $request
     * @return Response
     *
     * @OA\Post(
     *      path="/password/resetlink",
     *      summary="Request a password reset link to change on web.",
     *      tags={"User"},
     *      description="Send email to user to reset the password from the web or use the code for mobile.",
     *      @OA\RequestBody(
     *          description="Email for the user",
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function sendResetLinkEmail(Request $request)
    {
        $request->validate([
            'login_field' => 'required|string',
        ]);
        $fieldType = filter_var($request->login_field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $user = User::where($fieldType, $request->login_field)->first();
        if (!$user)
            return $this->sendError(
                __('passwords.user'),
                401
            );

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->login_field],
            [
                'email' => $user->phone,
                'token' => random_int(1000, 9999)
            ]
        );
        if ($user && $passwordReset){
            $fieldType == 'phone' ?
            $this->sendSms($passwordReset) :
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }

        return $this->sendResponse(
            $request->login_field,
            __('passwords.sent')
        );
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    /**
     * @param Request $request
     * @return Response
     *
     * @OA\Get(
     *      path="/password/find/{token}",
     *      summary="Check validity of reset token.",
     *      tags={"User"},
     *      description="Check if the token is not expired",
     *      @OA\Parameter(
     *          name="token",
     *          description="token of User",
     *          required=true,
     *          in="path",
     *              @OA\JsonContent(
     *                  type="string"
     *              )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return $this->sendError(
                __('passwords.token'), 401
            );
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return $this->sendError(
                __('passwords.token'), 401
            );
        }
        return $this->sendResponse(
            $passwordReset,
            __('passwords.tokenValid')
        );
    }
    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    /**
     * @param Request $request
     * @return Response
     *
     * @OA\Post(
     *      path="/password/reset",
     *      summary="Reset the password of the Users.",
     *      tags={"User"},
     *      description="Reset the password of the Users.",
     *      @OA\RequestBody(
     *          description="The Email and Password of the user and the new password with the token",
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="password_confirmation",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="token",
     *                  type="string"
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function reset(ResetUserAPIRequest $request)
    {
        $fieldType = filter_var($request->login_field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $user = User::where($fieldType, $request->login_field)->first();
        if (!$user)
            return $this->sendError(
                __('passwords.user'),
                401
            );
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $user->phone]
        ])->first();
        if (!$passwordReset || Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast())
            return $this->sendError(
                __('passwords.token'), 401
            );
        $user->password = $request->password;
        $user->save();
        $passwordReset->delete();

        if ($fieldType == 'email')
            $user->notify(new PasswordResetSuccess($passwordReset));

        return $this->sendResponse(
            $user,
            __('passwords.reset')
        );
    }

    public function sendSms($passwordReset)
    {
        $accountSid = config('twilio.TWILIO_ACCOUNT_SID');
        $authToken  = config('twilio')['TWILIO_AUTH_TOKEN'];
        $appSid     = config('twilio')['TWILIO_APP_SID'];
        $from       = config('twilio')['TWILIO_NUMBER'];
        $client = new Client($accountSid, $authToken);
        try {
            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
                // the number you'd like to send the message to
                '+'.$passwordReset->email,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $from,
                    // the body of the text message you'd like to send
                    'body' => 'Hey! your reset code is: ' . $passwordReset->token
                )
            );
        } catch (\Exception $e) {
            throw new MyApiException($e->getMessage(), 422);
        }
    }
}
