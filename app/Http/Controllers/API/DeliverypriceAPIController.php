<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliverypriceAPIRequest;
use App\Http\Requests\API\UpdateDeliverypriceAPIRequest;
use App\Models\Deliveryprice;
use App\Repositories\DeliverypriceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DeliverypriceController
 * @package App\Http\Controllers\API
 */

class DeliverypriceAPIController extends AppBaseController
{
    /** @var  DeliverypriceRepository */
    private $deliverypriceRepository;

    public function __construct(DeliverypriceRepository $deliverypriceRepo)
    {
        $this->deliverypriceRepository = $deliverypriceRepo;
    }

    /**
     * Display a listing of the Deliveryprice.
     * GET|HEAD /deliveryprices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryprices = $this->deliverypriceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $deliveryprices->toArray(),
            __('messages.retrieved', ['model' => __('models/deliveryprices.plural')])
        );
    }

    /**
     * Store a newly created Deliveryprice in storage.
     * POST /deliveryprices
     *
     * @param CreateDeliverypriceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliverypriceAPIRequest $request)
    {
        $input = $request->all();

        $deliveryprice = $this->deliverypriceRepository->create($input);

        return $this->sendResponse(
            $deliveryprice->toArray(),
            __('messages.saved', ['model' => __('models/deliveryprices.singular')])
        );
    }

    /**
     * Display the specified Deliveryprice.
     * GET|HEAD /deliveryprices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Deliveryprice $deliveryprice */
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/deliveryprices.singular')])
            );
        }

        return $this->sendResponse(
            $deliveryprice->toArray(),
            __('messages.retrieved', ['model' => __('models/deliveryprices.singular')])
        );
    }

    /**
     * Update the specified Deliveryprice in storage.
     * PUT/PATCH /deliveryprices/{id}
     *
     * @param int $id
     * @param UpdateDeliverypriceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliverypriceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Deliveryprice $deliveryprice */
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/deliveryprices.singular')])
            );
        }

        $deliveryprice = $this->deliverypriceRepository->update($input, $id);

        return $this->sendResponse(
            $deliveryprice->toArray(),
            __('messages.updated', ['model' => __('models/deliveryprices.singular')])
        );
    }

    /**
     * Remove the specified Deliveryprice from storage.
     * DELETE /deliveryprices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Deliveryprice $deliveryprice */
        $deliveryprice = $this->deliverypriceRepository->find($id);

        if (empty($deliveryprice)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/deliveryprices.singular')])
            );
        }

        $deliveryprice->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/deliveryprices.singular')])
        );
    }
}
