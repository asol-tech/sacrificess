<?php

namespace App\Http\Controllers\API;

use App\Exceptions\MyApiException;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\LoginUserAPIRequest;
use App\Http\Requests\API\VerifyUserAPIRequest;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Role;
use Response;
use Twilio\Rest\Client;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class MowasalenAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (auth()->id() != $id)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        return $this->sendResponse(
            $user->only('id', 'name', 'phone', 'city_name'),
            __('messages.retrieved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        if (auth()->id() != $id)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        $input = array_filter($request->only('name'));

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        $user = $this->userRepository->update($input, $id);

        if ($request->device_token) {
            $user->devicetokens()->updateOrCreate([
                'token' => $request->device_token
            ]);
        }

        return $this->sendResponse(
            $user->only('id', 'name', 'phone', 'city_name'),
            __('messages.updated', ['model' => __('models/users.singular')])
        );
    }


    public function login_mowasalen(LoginUserAPIRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        if (!$user /* || $user->hasRole('mowasalen') */)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        $user->verify_code = (trim($request->phone, '+') == '966548000000') ? 1234 : random_int(1000, 9999);
        $user->save();
        $this->sendSms($user);

        if ($request->device_token) {
            $user->devicetokens()->updateOrCreate([
                'token' => $request->device_token
            ]);
        }

        return $this->sendResponse(
            $user->only(['id', 'name', 'phone', 'city_name']),
            __('messages.saved', ['model' => __('models/users.singular')])
        );
    }

    // public function register(CreateUserAPIRequest $request)
    // {
    //     $input = $request->only(['name', 'email', 'password', 'phone']);
    //     $input['password'] = $request->phone;

    //     \DB::transaction(function () use ($request, $input, &$user) {
    //         $user = $this->userRepository->create($input);
    //         $user->attachRole('user');
    //         $this->sendSms($user);
    //     });

    //     return Response::json([
    //         'success' => true,
    //         'message' => __('messages.saved', ['model' => __('models/users.singular')]),
    //         'data' => $user->only(['id', 'name', 'phone'])
    //     ], 200);
    // }

    public function verify_code(VerifyUserAPIRequest $request)
    {
        $fieldType = filter_var($request->login_field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $user = User::where($fieldType, $request->login_field)->firstOrFail();

        if ($request->verify_code != $user->verify_code)
            return $this->sendError(
                __('auth.verify_code_false'),
                403
            );

        $user->phone_verified_at = now();
        $user->save();
        $token = $user->createToken('Laravel Password Grant User')->accessToken;
        $user->token = $token;
        $user->token_type = 'Bearer';

        if ($request->device_token){
            $user->devicetokens()->updateOrCreate([
                'token' => $request->device_token
            ]);
        }

        return $this->sendResponse(
            $user->only('id', 'name', 'phone', 'city_name', 'token', 'token_type'),
            __('auth.verified')
        );
    }

    public function logout(Request $request)
    {

        // $request->validate([
        //     'device_token' => 'required'
        // ]);

        $user = auth()->user();

        $user->token()->revoke();

        $token = $user->devicetokens()->where('token', $request->device_token)->first();

        if ($token) $token->delete();

        return $this->sendResponse(
            __('auth.sign_out'),
            __('auth.sign_out')
        );
    }

    public function sendSms($passwordReset)
    {
        if (trim($passwordReset->phone, '+') == '966548000000')
            return true;

        $accountSid = config('twilio.TWILIO_ACCOUNT_SID');
        $authToken  = config('twilio')['TWILIO_AUTH_TOKEN'];
        $appSid     = config('twilio')['TWILIO_APP_SID'];
        $from       = config('twilio')['TWILIO_NUMBER'];
        $client = new Client($accountSid, $authToken);
        try {
            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
                // the number you'd like to send the message to
                '+' . trim($passwordReset->phone, '+'),
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $from,
                    // the body of the text message you'd like to send
                    'body' => 'Hey! your activation code is: ' . $passwordReset->verify_code
                )
            );
        } catch (\Exception $e) {
            throw new MyApiException($e->getMessage(), 422);
        }
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return Response
     */
    public function orders(Request $request)
    {
        $orders = auth()->user()->orders()->with('client', 'orderitems')->paginate();

        $data = $orders->getCollection()->append('city_name', 'client')->map(function ($row) {
            $row->orderitems = $row->orderitems->map(function ($roy) {
                $roy->setVisible(['id', 'item', 'price', 'created_at', 'updated_at']);
                // $roy->makeHidden('order_id', 'size', 'quantity', 'choping', 'cuting', 'packing', 'head', 'notes', 'created_at', 'updated_at');
                return $roy;
            });
            return $row;
        });

        // dd($data);

        return $this->sendCResponse(
            [
                'data' => $data,
                'paginate' => [
                'current_page' => $orders->currentPage(),
                'total_items' => $orders->total(),
                'total_pages' => $orders->lastPage(),
                'per_oage' => 15
                ]
            ]
            ,
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    public function newOrders(Request $request)
    {
        $orders = Order::where('staff_id', NULL)->with('client', 'orderitems')->paginate();

        return $this->sendCResponse(
            [
                'data' => $orders->getCollection()->toArray(),
                'paginate' => [
                    'current_page' => $orders->currentPage(),
                    'total_items' => $orders->total(),
                    'total_pages' => $orders->lastPage(),
                    'per_oage' => 15
                ]
            ],
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    public function acceptOrders(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->staff_id = auth()->id();
        $order->save();

        return $this->sendResponse(
            $order,
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    public function deliverOrders(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        if ($order->staff_id != auth()->id())
            return $this->sendError(
                __('auth.failed'),
                401
            );

        $order->statu = 'delivered';
        $order->save();

        return $this->sendResponse(
            $order,
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    public function notifications(Request $request)
    {
        if ($request->test){
            $notifications = User::find(3)->notifies;
        } else {
            $notifications = auth()->user()->notifies;
        }

        return $this->sendResponse(
            $notifications,
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    public function notificationsRead(Notification $notification)
    {
        $notification->update(['read_at' => now()]);

        return $this->sendResponse(
            $notification,
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

}
