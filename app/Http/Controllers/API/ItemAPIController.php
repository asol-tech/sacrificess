<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateItemAPIRequest;
use App\Http\Requests\API\UpdateItemAPIRequest;
use App\Models\Item;
use App\Repositories\ItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\City;
use Response;

/**
 * Class ItemController
 * @package App\Http\Controllers\API
 */

class ItemAPIController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     * GET|HEAD /items
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $items = $this->itemRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        $request->validate([
            'city_id' => 'required|integer'
        ], $request->all());

        $items = City::find($request->city_id)->items;

        return $this->sendResponse(
            $items->toArray(),
            __('messages.retrieved', ['model' => __('models/items.plural')])
        );
    }

    /**
     * Store a newly created Item in storage.
     * POST /items
     *
     * @param CreateItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateItemAPIRequest $request)
    {
        $input = $request->all();

        $item = $this->itemRepository->create($input);

        return $this->sendResponse(
            $item->toArray(),
            __('messages.saved', ['model' => __('models/items.singular')])
        );
    }

    /**
     * Display the specified Item.
     * GET|HEAD /items/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        request()->validate([
            'city_id' => 'required|integer'
        ], request()->all());

        /** @var Item $item */
        $item = $this->itemRepository->find($id)->load('sizes', 'additions');

        if (empty($item)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/items.singular')])
            );
        }

        return $this->sendResponse(
            $item->toArray(),
            __('messages.retrieved', ['model' => __('models/items.singular')])
        );
    }

    /**
     * Update the specified Item in storage.
     * PUT/PATCH /items/{id}
     *
     * @param int $id
     * @param UpdateItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var Item $item */
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/items.singular')])
            );
        }

        $item = $this->itemRepository->update($input, $id);

        return $this->sendResponse(
            $item->toArray(),
            __('messages.updated', ['model' => __('models/items.singular')])
        );
    }

    /**
     * Remove the specified Item from storage.
     * DELETE /items/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Item $item */
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/items.singular')])
            );
        }

        $item->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/items.singular')])
        );
    }
}
