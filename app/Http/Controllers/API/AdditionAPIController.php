<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdditionAPIRequest;
use App\Http\Requests\API\UpdateAdditionAPIRequest;
use App\Models\Addition;
use App\Repositories\AdditionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AdditionController
 * @package App\Http\Controllers\API
 */

class AdditionAPIController extends AppBaseController
{
    /** @var  AdditionRepository */
    private $additionRepository;

    public function __construct(AdditionRepository $additionRepo)
    {
        $this->additionRepository = $additionRepo;
    }

    /**
     * Display a listing of the Addition.
     * GET|HEAD /additions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $additions = $this->additionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $additions->toArray(),
            __('messages.retrieved', ['model' => __('models/additions.plural')])
        );
    }

    /**
     * Store a newly created Addition in storage.
     * POST /additions
     *
     * @param CreateAdditionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAdditionAPIRequest $request)
    {
        $input = $request->all();

        $addition = $this->additionRepository->create($input);

        return $this->sendResponse(
            $addition->toArray(),
            __('messages.saved', ['model' => __('models/additions.singular')])
        );
    }

    /**
     * Display the specified Addition.
     * GET|HEAD /additions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Addition $addition */
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/additions.singular')])
            );
        }

        return $this->sendResponse(
            $addition->toArray(),
            __('messages.retrieved', ['model' => __('models/additions.singular')])
        );
    }

    /**
     * Update the specified Addition in storage.
     * PUT/PATCH /additions/{id}
     *
     * @param int $id
     * @param UpdateAdditionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdditionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Addition $addition */
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/additions.singular')])
            );
        }

        $addition = $this->additionRepository->update($input, $id);

        return $this->sendResponse(
            $addition->toArray(),
            __('messages.updated', ['model' => __('models/additions.singular')])
        );
    }

    /**
     * Remove the specified Addition from storage.
     * DELETE /additions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Addition $addition */
        $addition = $this->additionRepository->find($id);

        if (empty($addition)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/additions.singular')])
            );
        }

        $addition->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/additions.singular')])
        );
    }
}
