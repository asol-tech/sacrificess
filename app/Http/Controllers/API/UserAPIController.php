<?php

namespace App\Http\Controllers\API;

use App\Exceptions\MyApiException;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\LoginUserAPIRequest;
use App\Http\Requests\API\VerifyUserAPIRequest;
use App\Models\Role;
use Response;
use Twilio\Rest\Client;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $users->toArray(),
            __('messages.retrieved', ['model' => __('models/users.plural')])
        );
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        return $this->sendResponse(
            $user->toArray(),
            __('messages.saved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (auth()->id() != $id)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        return $this->sendResponse(
            $user->only('id', 'name', 'phone'),
            __('messages.retrieved', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        if (auth()->id() != $id)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        $input = array_filter($request->only('name'));

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse(
            $user->only('id', 'name', 'phone'),
            __('messages.updated', ['model' => __('models/users.singular')])
        );
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/users.singular')])
            );
        }

        $user->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/users.singular')])
        );
    }


    public function login_mowasalen(LoginUserAPIRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        if (!$user /* || $user->hasRole('mowasalen') */)
            return $this->sendError(
                __('auth.failed'),
                401
            );

        $user->verify_code = (trim($request->phone, '+') == '966548000000') ? 1234 : random_int(1000, 9999);
        $user->save();
        $this->sendSms($user);
        return $this->sendResponse(
            $user->only(['id', 'name', 'phone']),
            __('messages.saved', ['model' => __('models/users.singular')])
        );
    }

    public function login(LoginUserAPIRequest $request)
    {
        $fieldType = filter_var($request->login_field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $user = User::where($fieldType, $request->login_field)->first();

        if ($user) {
            // if (\Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant User')->accessToken;
                $user->token = $token;
                $user->token_type = 'Bearer';
                return $this->sendResponse(
                    $user->only(['id', 'name', 'phone', 'token', 'token_type']),
                    __('auth.success')
                );
            // } else {
            //     return $this->sendError(
            //         __('auth.failed'),
            //         401
            //     );
            // }
        } else {
            return $this->sendError(
                __('auth.failed'),
                401
            );
        }
    }

    public function register(CreateUserAPIRequest $request)
    {
        $input = $request->only(['name', 'email', 'password', 'phone']);
        $input['password'] = $request->phone;

        \DB::transaction(function () use ($request, $input, &$user) {
            $user = $this->userRepository->create($input);
            $user->attachRole('user');
            $this->sendSms($user);
        });

        return Response::json([
            'success' => true,
            'message' => __('messages.saved', ['model' => __('models/users.singular')]),
            'data' => $user->only(['id', 'name', 'phone'])
        ], 200);
    }

    public function user_auth(LoginUserAPIRequest $request)
    {
        $input = $request->only(['name', 'email', 'password', 'phone']);

        $input['password'] = $request->password ?? $request->phone;
        $input['verify_code'] = (trim($request->phone, '+') == '966548000000') ? 1234 : random_int(1000, 9999);

        \DB::transaction(function () use ($request, $input, &$user) {
            $user = User::updateOrCreate(
                ['phone' => $request->phone],
                $input
            );
            if (!$user->hasRole('user')) $user->attachRole('user');
            $this->sendSms($user);
        });

        return Response::json([
            'success' => true,
            'message' => __('messages.saved', ['model' => __('models/users.singular')]),
            'data' => $user->only(['id', 'name', 'phone'])
        ], 200);
    }

    public function verify_code(VerifyUserAPIRequest $request)
    {
        $fieldType = filter_var($request->login_field, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $user = User::where($fieldType, $request->login_field)->firstOrFail();

        $request->validate([
            'verify_code' => 'required',
            'login_field' => 'required',
        ]);
        if ($request->verify_code != $user->verify_code)
            return $this->sendError(
                __('auth.verify_code_false'),
                403
            );

        $user->phone_verified_at = now();
        $user->save();
        $token = $user->createToken('Laravel Password Grant User')->accessToken;
        $user->token = $token;
        $user->token_type = 'Bearer';

        return $this->sendResponse(
            $user->only('id', 'name', 'phone', 'token', 'token_type'),
            __('auth.verified')
        );
    }

    public function sendSms($passwordReset)
    {
        if (trim($passwordReset->phone, '+') == '966548000000')
            return true;

        $accountSid = config('twilio.TWILIO_ACCOUNT_SID');
        $authToken  = config('twilio')['TWILIO_AUTH_TOKEN'];
        $appSid     = config('twilio')['TWILIO_APP_SID'];
        $from       = config('twilio')['TWILIO_NUMBER'];
        $client = new Client($accountSid, $authToken);
        try {
            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
                // the number you'd like to send the message to
                '+' . trim($passwordReset->phone, '+'),
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $from,
                    // the body of the text message you'd like to send
                    'body' => 'Hey! your activation code is: ' . $passwordReset->verify_code
                )
            );
        } catch (\Exception $e) {
            throw new MyApiException($e->getMessage(), 422);
        }
    }
}
