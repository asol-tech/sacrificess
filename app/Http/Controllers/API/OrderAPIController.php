<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderAPIRequest;
use App\Http\Requests\API\UpdateOrderAPIRequest;
use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Addition;
use App\Models\Apidebug;
use App\Models\Deliveryprice;
use App\Models\Item;
use App\Models\Size;
use phpDocumentor\Reflection\Types\Null_;
use Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */

class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $orders = $this->orderRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // )->load('orderitems');
        $orders = auth()->user()->load('orders.orderitems')->orders;

        return $this->sendResponse(
            $orders->toArray(),
            __('messages.retrieved', ['model' => __('models/orders.plural')])
        );
    }

    /**
     * Store a newly created Order in storage.
     * POST /orders
     *
     * @param CreateOrderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderAPIRequest $request)
    {
        // Apidebug::create([
        //     'requset_header' => json_encode($request->all()),
        //     'requset_body' => json_encode($request->header()),
        //     'requset' => $request
        // ]);

        $input = $request->all();
        $input['delivery_price'] = Deliveryprice::where('city_id', $input['city_id'])->firstOrFail()->price;
        $input['client_id'] = auth()->id();

        $order = $this->orderRepository->create($input);

        $totalPrice = $input['delivery_price'];

        foreach (json_decode($input['items'], true) as $item){

            $item['item_id'] = $item['item_id'] ?? $item['id'];
            $theItem = Item::find($item['item_id']);
            $size = Size::find($item['size_id']);
            $head = Addition::find($item['head'] ?? 0);
            $cuting = Addition::find($item['cuting'] ?? 0);
            $packing = Addition::find($item['packing'] ?? 0);
            $choping = Addition::find($item['choping'] ?? 0);

            $item['price'] = ( (($size->price ?? 0) + ($head->price ?? 0) + ($cuting->price ?? 0) + ($packing->price ?? 0) + ($choping->price ?? 0)) * $item['quantity'] );

            $totalPrice += $item['price'];

            $item['head'] = $head->name ?? '';
            $item['cuting'] = $cuting->name ?? '';
            $item['packing'] = $packing->name ?? '';
            $item['size'] = $size->name ?? '';
            $item['choping'] = $choping->name ?? '';
            $item['item'] = $theItem->name ?? '';

            $order->orderitems()->create($item);
        }

        $order->update(['price' => $totalPrice]);

        return $this->sendResponse(
            $order->load('orderitems')->toArray(),
            __('messages.saved', ['model' => __('models/orders.singular')])
        );
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orders.singular')])
            );
        }

        return $this->sendResponse(
            $order->toArray(),
            __('messages.retrieved', ['model' => __('models/orders.singular')])
        );
    }

    /**
     * Update the specified Order in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orders.singular')])
            );
        }

        $order = $this->orderRepository->update($input, $id);

        return $this->sendResponse(
            $order->toArray(),
            __('messages.updated', ['model' => __('models/orders.singular')])
        );
    }

    /**
     * Remove the specified Order from storage.
     * DELETE /orders/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orders.singular')])
            );
        }

        $order->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/orders.singular')])
        );
    }
}
