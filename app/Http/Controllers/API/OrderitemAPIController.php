<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderitemAPIRequest;
use App\Http\Requests\API\UpdateOrderitemAPIRequest;
use App\Models\Orderitem;
use App\Repositories\OrderitemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderitemController
 * @package App\Http\Controllers\API
 */

class OrderitemAPIController extends AppBaseController
{
    /** @var  OrderitemRepository */
    private $orderitemRepository;

    public function __construct(OrderitemRepository $orderitemRepo)
    {
        $this->orderitemRepository = $orderitemRepo;
    }

    /**
     * Display a listing of the Orderitem.
     * GET|HEAD /orderitems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderitems = $this->orderitemRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $orderitems->toArray(),
            __('messages.retrieved', ['model' => __('models/orderitems.plural')])
        );
    }

    /**
     * Store a newly created Orderitem in storage.
     * POST /orderitems
     *
     * @param CreateOrderitemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderitemAPIRequest $request)
    {
        $input = $request->all();

        $orderitem = $this->orderitemRepository->create($input);

        return $this->sendResponse(
            $orderitem->toArray(),
            __('messages.saved', ['model' => __('models/orderitems.singular')])
        );
    }

    /**
     * Display the specified Orderitem.
     * GET|HEAD /orderitems/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Orderitem $orderitem */
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orderitems.singular')])
            );
        }

        return $this->sendResponse(
            $orderitem->toArray(),
            __('messages.retrieved', ['model' => __('models/orderitems.singular')])
        );
    }

    /**
     * Update the specified Orderitem in storage.
     * PUT/PATCH /orderitems/{id}
     *
     * @param int $id
     * @param UpdateOrderitemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderitemAPIRequest $request)
    {
        $input = $request->all();

        /** @var Orderitem $orderitem */
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orderitems.singular')])
            );
        }

        $orderitem = $this->orderitemRepository->update($input, $id);

        return $this->sendResponse(
            $orderitem->toArray(),
            __('messages.updated', ['model' => __('models/orderitems.singular')])
        );
    }

    /**
     * Remove the specified Orderitem from storage.
     * DELETE /orderitems/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Orderitem $orderitem */
        $orderitem = $this->orderitemRepository->find($id);

        if (empty($orderitem)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/orderitems.singular')])
            );
        }

        $orderitem->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/orderitems.singular')])
        );
    }
}
