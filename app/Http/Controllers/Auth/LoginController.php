<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        config([
            'services.' . $provider . '.client_id' => settings()->get($provider . '_client_id'),
            'services.' . $provider . '.client_secret' => settings()->get($provider . '_client_secret'),
            'services.' . $provider . '.redirect_url' => settings()->get($provider . '_redirect_url'),
        ]);
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {

            // $social_user = Socialite::driver($provider)->stateless()->user();
            $social_user = Socialite::driver($provider)->user();
        } catch (Exception $e) {

            return redirect('/');
        }

        $user = User::where('provider', $provider)
            ->where('provider_id', $social_user->getId())
            ->first();

        if (!$user) {

            $user = User::create([
                'name' => $social_user->getName(),
                'email' => $social_user->getEmail(),
                'provider' => $provider,
                'provider_id' => $social_user->getId(),
            ]);

            $user->attachRole('user');
        } //end of if

        Auth::login($user, true);

        return redirect()->intended('/');

        // $user->token;
    } //end of handle callback

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login_field');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($user->statu == 'disabled') {
            \Auth::logout();
            abort(403, 'Blocked User');
        };
    }
}
