<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Item
 * @package App\Models
 * @version June 14, 2020, 8:08 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection countries
 * @property \Illuminate\Database\Eloquent\Collection sizes
 * @property \Illuminate\Database\Eloquent\Collection additions
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property string name
 * @property string image
 * @property boolean statu
 */
class Item extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, SecureDelete;

    public $table = 'items';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'image',
        'statu'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'image' => 'string',
        'statu' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'image' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function cities()
    {
        return $this->belongsToMany(\App\Models\City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function sizes()
    {
        return $this->belongsToMany(\App\Models\Size::class)
            ->when(request()->city_id, function ($q) {
                return $q->whereHas('cities', function ($qq) {
                    return $qq->where('id', request()->city_id);
                });
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function additions()
    {
        return $this->belongsToMany(\App\Models\Addition::class)
            ->when(request()->city_id, function ($q) {
                return $q->whereHas('cities', function($qq){
                    return $qq->where('id', request()->city_id);
                });
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/backend-files/assets/images/big/img3.jpg');
    }

    /* Accessors */
    public function getImageAttribute()
    {
        return url($this->getFirstMediaUrl('image'));
    }
}
