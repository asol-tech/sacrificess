<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public $guarded = [];

    public $fillable = [
        'name',
        'display_name',
        'description'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[a-zA-Z0-9\\s]+$/|unique:roles,name',
        'display_name' => 'required',
        'description' => 'required',
        'permissions' => 'required|array|min:1'
    ];

    public function scopeRoleNot($query, $role_name)
    {
        return $query->whereNotIn('name', (array) $role_name);
    }// end of scopeWhereRoleNot
}
