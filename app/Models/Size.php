<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Size
 * @package App\Models
 * @version June 14, 2020, 9:41 am UTC
 *
 * @property \App\Models\Item item
 * @property \App\Models\City city
 * @property string name
 * @property string size
 * @property number price
 */
class Size extends Model
{
    use SoftDeletes;

    public $table = 'sizes';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'size',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'size' => 'string',
        'price' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'size' => 'required',
        'cities' => 'nullable|array',
        'cities.*' => 'integer',
        'items' => 'nullable|array',
        'items.*' => 'integer',
        'price' => 'required'
    ];

    public function items()
    {
        return $this->belongsToMany(\App\Models\Item::class);
    }

    public function cities()
    {
        return $this->belongsToMany(\App\Models\City::class);
    }
}
