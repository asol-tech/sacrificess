<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orderitem
 * @package App\Models
 * @version June 23, 2020, 5:51 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection items
 * @property \Illuminate\Database\Eloquent\Collection sizes
 * @property integer order_id
 * @property integer item_id
 * @property integer size_id
 * @property integer quantity
 * @property integer choping
 * @property integer cuting
 * @property integer packing
 * @property integer head
 * @property string notes
 */
class Orderitem extends Model
{
    use SoftDeletes;

    public $table = 'orderitems';

    protected $dates = ['deleted_at'];

    protected $hidden = ['deleted_at'];

    public $fillable = [
        'order_id',
        'item',
        'size',
        'quantity',
        'choping',
        'cuting',
        'packing',
        'head',
        'price',
        'notes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_id' => 'integer',
        'item' => 'string',
        'size' => 'string',
        'quantity' => 'string',
        'choping' => 'string',
        'cuting' => 'string',
        'packing' => 'string',
        'head' => 'string',
        'price' => 'double',
        'notes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required|integer',
        'item' => 'required',
        'size' => 'required',
        'quantity' => 'required|integer',
        'choping' => 'nullable',
        'cuting' => 'nullable',
        'packing' => 'nullable',
        'head' => 'nullable',
        'price' => 'nullable',
        'notes' => 'nullable'
    ];

    public function order()
    {
        return $this->belongsTo(\App\Models\order::class);
    }

    /* public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    public function size()
    {
        return $this->belongsTo(\App\Models\Size::class);
    }

    public function choping()
    {
        return $this->belongsTo(\App\Models\Addition::class, 'choping');
    }

    public function cuting()
    {
        return $this->belongsTo(\App\Models\Addition::class, 'cuting');
    }

    public function packing()
    {
        return $this->belongsTo(\App\Models\Addition::class, 'packing');
    }

    public function head()
    {
        return $this->belongsTo(\App\Models\Addition::class, 'head');
    } */

}
