<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 * @version June 14, 2020, 12:04 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cities
 * @property \Illuminate\Database\Eloquent\Collection users
 * @property \Illuminate\Database\Eloquent\Collection user1s
 * @property \Illuminate\Database\Eloquent\Collection orderitems
 * @property integer client_id
 * @property integer staff_id
 * @property integer city_id
 * @property string address
 * @property string payment
 * @property number delivery_price
 * @property string day
 * @property string time
 * @property string statu
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';

    protected $dates = ['deleted_at'];

    protected $hidden = ['deleted_at'];

    // protected $appends = ['city_name'];

    public $fillable = [
        'client_id',
        'staff_id',
        'city_id',
        'address',
        'lat',
        'long',
        'payment',
        'delivery_price',
        'price',
        'day',
        'time',
        'statu'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'integer',
        'staff_id' => 'integer',
        'city_id' => 'integer',
        'address' => 'string',
        'lat' => 'integer',
        'long' => 'integer',
        'payment' => 'string',
        'delivery_price' => 'double',
        'price' => 'double',
        // 'day' => 'date:d-m-y',
        'statu' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'client_id' => 'required',
        'staff_id' => 'nullable',
        'city_id' => 'required',
        'address' => 'required',
        'lat' => 'nullable',
        'long' => 'nullable',
        'payment' => 'required',
        'delivery_price' => 'required',
        'price' => 'nullable',
        'day' => 'date',
        'time' => 'required', //'date_format:H:i:s',
        'statu' => 'nullable'
    ];

    public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\User::class, 'client_id');
    }

    public function staff()
    {
        return $this->belongsTo(\App\Models\User::class, 'staff_id');
    }

    public function orderitems()
    {
        return $this->hasMany(\App\Models\Orderitem::class);
    }

    /* Accessors */
    public function getCityNameAttribute()
    {
        return $this->city->name;
    }

}
