<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, Notifiable, LaratrustUserTrait, HasMediaTrait;

    /**
     * Delete only when there is no reference to other models.
     *
     * @return response
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->verify_code = random_int(1000, 9999);
        });

        static::deleting(function ($modelDelete) {
            $relationMethods = $modelDelete->relations;

            foreach ($relationMethods as $relationMethod) {
                if ($modelDelete->$relationMethod()->count() > 0) {
                    return false;
                }
            }
        });

    }

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'verify_code', 'statu', 'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $relations = ['orders'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'statu' => 'string',
        'phone' => 'integer',
        'city_id' => 'integer',
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable',
        'email' => 'nullable|email|unique:users,email',
        'phone' => 'required|phone:SA|unique:users,phone',
        'password' => 'nullable|min:6|confirmed',
        'role' => 'required|integer',
        'statu' => 'nullable',
        'city_id' => 'nullable|integer',
    ];
    public static $login_rules = [
        'name' => 'nullable',
        'phone' => 'required|phone:SA|string|max:255',
    ];

    public function setPasswordAttribute($password)
    {
        if (!is_null($password) && strlen($password) == 60 && preg_match('/^\$2y\$/', $password)) {
            $this->attributes['password'] = $password;
        } else {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/front/images/uploads/user-img.png');
    }

    /* Relations */
    public function orders()
    {
        $f = auth()->user()->hasRole('mowasalen') ? 'staff_id' : 'client_id';
        return $this->hasMany(\App\Models\Order::class, $f)->orderBy('id', 'desc');
    }
    public function devicetokens()
    {
        return $this->hasMany(\App\Models\Devicetoken::class);
    }
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }

    public function notifies()
    {
        return $this->morphMany(\App\Models\Notification::class, 'notifiable');
    }



    /* Accessors */
    public function getImageAttribute()
    {
        return url($this->getFirstMediaUrl('image'));
    }

    public function getCityNameAttribute()
    {
        return $this->city->name;
    }

    /* Scoopes */
    public function scopeWhenRole($query, $role)
    {
        return $query->when($role, function ($q) use ($role) {
            return $q->whereRoleIs($role);
        });
    }

    public function scopeWhenRoleNot($query, $role)
    {
        return $query->when($role, function ($q) use ($role) {
            return $q->whereRoleIs($role);
        });
    }

}
