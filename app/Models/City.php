<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class City
 * @package App\Models
 * @version June 13, 2020, 2:15 pm UTC
 *
 * @property string name
 * @property string image
 * @property boolean statu
 */
class City extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, SecureDelete;

    public $table = 'cities';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'image',
        'statu'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'image' => 'string',
        'statu' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'image' => 'nullable'
    ];

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/backend-files/assets/images/big/img3.jpg');
    }

    /* Accessors */
    public function getImageAttribute()
    {
        return url($this->getFirstMediaUrl('image'));
    }

    public function items()
    {
        return $this->belongsToMany(\App\Models\Item::class);
    }
}
