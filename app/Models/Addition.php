<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Addition
 * @package App\Models
 * @version June 14, 2020, 11:00 am UTC
 *
 * @property \App\Models\Item item
 * @property \App\Models\City city
 * @property string name
 * @property number price
 * @property string type
 * @property boolean statu
 */
class Addition extends Model
{
    use SoftDeletes;

    public $table = 'additions';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'price',
        'type',
        'statu'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'price' => 'double',
        'type' => 'string',
        'statu' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'price' => 'required',
        'type' => 'required',
        'cities' => 'nullable|array',
        'cities.*' => 'integer',
        'items' => 'nullable|array',
        'items.*' => 'integer'
    ];

    public function items()
    {
        return $this->belongsToMany(\App\Models\Item::class);
    }

    public function cities()
    {
        return $this->belongsToMany(\App\Models\City::class);
    }
}
