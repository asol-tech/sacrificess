<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Deliveryprice
 * @package App\Models
 * @version June 14, 2020, 11:38 am UTC
 *
 * @property \App\Models\City city
 * @property integer city_id
 * @property number price
 */
class Deliveryprice extends Model
{
    use SoftDeletes;

    public $table = 'deliveryprices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'city_id',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'city_id' => 'integer',
        'price' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'city_id' => 'nullable',
        'price' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }
}
