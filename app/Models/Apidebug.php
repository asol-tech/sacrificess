<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apidebug extends Model
{
    public $fillable = [
        'requset_header',
        'requset_body',
        'requset'
    ];
}
