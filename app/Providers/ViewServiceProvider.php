<?php

namespace App\Providers;

use App\Models\Addition;
use App\Models\Order;
use App\Models\Size;
use App\Models\User;
use App\Models\Item;
use Illuminate\Support\ServiceProvider;
use View;
use App\Models\City;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['backend.orderitems.fields'], function ($view) {
            $orderItems = Order::pluck('id','id')->toArray();
            $itemItems = Item::pluck('name','name')->toArray();
            $sizeItems = Size::pluck('name','name')->toArray();
            $chopingItems = Addition::where('type', 'choping')->pluck('name','name')->toArray();
            $cutingItems = Addition::where('type', 'cuting')->pluck('name','name')->toArray();
            $packingItems = Addition::where('type', 'packing')->pluck('name','name')->toArray();
            $headItems = Addition::where('type', 'head')->pluck('name','name')->toArray();
            $view->with([
                'orderItems' => $orderItems,
                'itemItems' => $itemItems,
                'sizeItems' => $sizeItems,
                'chopingItems' => $chopingItems,
                'cutingItems' => $cutingItems,
                'packingItems' => $packingItems,
                'headItems' => $headItems,
                ]);
        });
        View::composer(['backend.orders.fields'], function ($view) {
            $cityItems = City::pluck('name','id')->toArray();
            $userItems = User::pluck('name','id')->toArray();
            $view->with(['userItems' => $userItems, 'cityItems' => $cityItems]);
        });
        View::composer(['backend.deliveryprices.fields', 'backend.items.fields'], function ($view) {
            $cityItems = City::pluck('name','id')->toArray();
            $view->with('cityItems', $cityItems);
        });
        View::composer(['backend.sizes.fields', 'backend.additions.fields'], function ($view) {
            $cityItems = City::pluck('name','id')->toArray();
            $itemItems = Item::pluck('name','id')->toArray();
            $view->with(['cityItems' => $cityItems, 'itemItems' => $itemItems]);
        });
        View::composer(['backend.users.fields'], function ($view) {
            $cityItems = City::pluck('name', 'id')->toArray();
            $view->with('cityItems', $cityItems);
        });
    }
}
