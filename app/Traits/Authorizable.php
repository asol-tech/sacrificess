<?php

namespace App\Traits;

trait Authorizable
{
    private $abilities = [
        'index' => 'read',
        'edit' => 'update',
        'show' => 'read',
        'update' => 'update',
        'create' => 'create',
        'store' => 'create',
        'destroy' => 'delete',
    ];

    /**
     * Override of callAction to perform the authorization before
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function callAction($method, $parameters)
    {
        // dd($this->getAbility($method));
        if (!auth()->user()->isAbleTo($this->getAbility($method))){
            abort(403, 'This Action is Forbidden');
        }
        return parent::callAction($method, $parameters);
    }

    public function getAbility($method)
    {
        $routeName = explode('.', \Request::route()->getName());
        $action = \Arr::get($this->getAbilities(), $method);

        return $action ? $action . '_' . $routeName[1] : null;
    }

    private function getAbilities()
    {
        return $this->abilities;
    }

    public function setAbilities($abilities)
    {
        $this->abilities = $abilities;
    }
}
