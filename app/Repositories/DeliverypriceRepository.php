<?php

namespace App\Repositories;

use App\Models\Deliveryprice;
use App\Repositories\BaseRepository;

/**
 * Class DeliverypriceRepository
 * @package App\Repositories
 * @version June 14, 2020, 11:38 am UTC
*/

class DeliverypriceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_id',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Deliveryprice::class;
    }
}
