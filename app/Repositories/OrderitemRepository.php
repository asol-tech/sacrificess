<?php

namespace App\Repositories;

use App\Models\Orderitem;
use App\Repositories\BaseRepository;

/**
 * Class OrderitemRepository
 * @package App\Repositories
 * @version June 23, 2020, 5:51 pm UTC
*/

class OrderitemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'item',
        'size',
        'quantity',
        'choping',
        'cuting',
        'packing',
        'head',
        'price',
        'notes'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orderitem::class;
    }
}
