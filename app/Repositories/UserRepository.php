<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version May 8, 2020, 2:46 pm UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'city_id',
        'statu',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }


    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $model = $this->model->newInstance($input);
        $model->save();

        if (request()->hasFile('image')) {
            $input['image'] = $model
                ->addMedia(request()->image)
                ->toMediaCollection('image')->getUrl();
        }

        if ($model->cities) {
            if (empty($input['cities'])) {
                $model->cities()->sync([]);
            } else {
                $model->cities()->sync($input['cities']);
            }
        }

        return $model;
    }
}
