<?php

namespace App\Repositories;

use App\Models\Addition;
use App\Repositories\BaseRepository;

/**
 * Class AdditionRepository
 * @package App\Repositories
 * @version June 14, 2020, 11:00 am UTC
*/

class AdditionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'type',
        'statu'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Addition::class;
    }
}
