<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'backend.orders.datatables_actions')
            // ->editColumn('day', function ($query) {
            //     return ($query->day ? $query->day->format('d/m/Y') : '');
            // })
            ->editColumn('staff_id', function ($query) {
                return $query->staff->name ?? __('site.not_assigned');
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery()->with(['city', 'client', 'staff']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => false,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/Arabic.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => '#', 'data' => 'id']),
            'client_id' => new Column(['title' => __('models/orders.fields.client_id'), 'data' => 'client.phone']),
            'staff_id' => new Column(['title' => __('models/orders.fields.staff_id'), 'data' => 'staff_id']),
            'city_id' => new Column(['title' => __('models/orders.fields.city_id'), 'data' => 'city.name']),
            'address' => new Column(['title' => __('models/orders.fields.address'), 'data' => 'address']),
            'payment' => new Column(['title' => __('models/orders.fields.payment'), 'data' => 'payment']),
            'delivery_price' => new Column(['title' => __('models/orders.fields.delivery_price'), 'data' => 'delivery_price']),
            'price' => new Column(['title' => __('models/orders.fields.price'), 'data' => 'price']),
            'day' => new Column(['title' => __('models/orders.fields.day'), 'data' => 'day']),
            'time' => new Column(['title' => __('models/orders.fields.time'), 'data' => 'time']),
            'statu' => new Column(['title' => __('models/orders.fields.statu'), 'data' => 'statu'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders_datatable_' . time();
    }
}
