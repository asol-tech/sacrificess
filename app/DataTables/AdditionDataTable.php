<?php

namespace App\DataTables;

use App\Models\Addition;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AdditionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'backend.additions.datatables_actions')
            ->editColumn('created_at', function ($query) {
                return ($query->created_at->format('d/m/Y'));
            })
            ->editColumn('type', function ($query) {
                return __('models/additions.fields.typeOptions.'.$query->type);
            })
            ->addColumn('cities', 'backend.datatables_cities')
            ->addColumn('items', 'backend.datatables_items')
            ->rawColumns(['action', 'cities', 'items'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Addition $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Addition $model)
    {
        return $model->newQuery()->with('cities', 'items');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => false,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/Arabic.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => '#', 'data' => 'id']),
            'name' => new Column(['title' => __('models/additions.fields.name'), 'data' => 'name']),
            'price' => new Column(['title' => __('models/additions.fields.price'), 'data' => 'price']),
            'type' => new Column(['title' => __('models/additions.fields.type'), 'data' => 'type']),
            'cities' => new Column(['title' => __('models/cities.plural'), 'data' => 'cities', 'searchable' => false]),
            'items' => new Column(['title' => __('models/items.plural'), 'data' => 'items', 'searchable' => false]),
            'statu' => new Column(['title' => __('models/additions.fields.statu'), 'data' => 'statu'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'additions_datatable_' . time();
    }
}
