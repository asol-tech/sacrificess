<?php

namespace App\DataTables;

use App\Models\Orderitem;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrderitemDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'backend.orderitems.datatables_actions')
            // ->editColumn('created_at', function ($query) {
            //     return ($query->created_at->format('d/m/Y'));
            // })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Orderitem $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Orderitem $model)
    {
        return $model->newQuery(); //->with('choping', 'cuting', 'packing', 'head', 'item');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => false,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/Arabic.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => '#', 'data' => 'id']),
            'order_id' => new Column(['title' => __('models/orderitems.fields.order_id'), 'data' => 'order_id']),
            'item' => new Column(['title' => __('models/orderitems.fields.item'), 'data' => 'item']),
            'size' => new Column(['title' => __('models/orderitems.fields.size'), 'data' => 'size']),
            'quantity' => new Column(['title' => __('models/orderitems.fields.quantity'), 'data' => 'quantity']),
            'choping' => new Column(['title' => __('models/orderitems.fields.choping'), 'data' => 'choping']),
            'cuting' => new Column(['title' => __('models/orderitems.fields.cuting'), 'data' => 'cuting']),
            'packing' => new Column(['title' => __('models/orderitems.fields.packing'), 'data' => 'packing']),
            'head' => new Column(['title' => __('models/orderitems.fields.head'), 'data' => 'head']),
            'notes' => new Column(['title' => __('models/orderitems.fields.notes'), 'data' => 'notes']),
            'price' => new Column(['title' => __('models/orderitems.fields.price'), 'data' => 'price'])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orderitems_datatable_' . time();
    }
}
