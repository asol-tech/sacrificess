<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/users.fields.id').':') !!}
    <p>{{ $user->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    <p>{{ $user->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', __('models/users.fields.phone').':') !!}
    <p>{{ $user->phone }}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', __('models/users.fields.city_id').':') !!}
    <p>{{ $user->city->name ?? '' }}</p>
</div>

<!-- Statu Field -->
<div class="form-group">
    {!! Form::label('statu', __('models/users.fields.statu').':') !!}
    <p>{{ __('models/users.fields.statuOptions.'.$user->statu) }}</p>
</div>

<!-- Phone Verify Field -->
<div class="form-group">
    {!! Form::label('phone', __('models/users.fields.phone_verified_at').':') !!}
    <p>{{ $user->phone_verified_at }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/users.fields.created_at').':') !!}
    <p>{{ $user->created_at }}</p>
</div>

