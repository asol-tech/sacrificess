<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', __('models/users.fields.phone').':') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_verified_at', __('models/users.fields.phone_verified_at').':') !!}
    {!! Form::date('phone_verified_at', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', __('models/cities.singular').':') !!}
    {!! Form::select('city_id', $cityItems, null, ['class' => 'form-control select2', 'placeholder' => __('site.select') . ' ' . __('models/cities.singular')]) !!}
</div>

<!-- Statu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('statu', __('models/users.fields.statu').':') !!}
    {!! Form::select('statu', ['pending' => __('models/users.fields.statuOptions.pending'), 'active' => __('models/users.fields.statuOptions.active'), 'disabled' => __('models/users.fields.statuOptions.disabled')], null, ['class' => 'form-control', 'placeholder' => __('site.select') . ' ' . __('models/users.fields.statu')]) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', __('models/users.fields.password').':') !!}
    {!! Form::text('password', '', ['class' => 'form-control']) !!}
</div>

<!-- Password Confirmation -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', __('models/users.fields.password_confirmation').':') !!}
    {!! Form::text('password_confirmation', null, ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', __('models/roles.singular')) !!}
    {!! Form::select('role', $roles, (isset($user) ? $user->roles->first()->id ?? '' : null), ['class' => 'form-control', 'placeholder' => __('site.please_select_role')]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.users.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
