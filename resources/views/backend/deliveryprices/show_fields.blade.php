<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/deliveryprices.fields.id').':') !!}
    <p>{{ $deliveryprice->id }}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', __('models/deliveryprices.fields.city_id').':') !!}
    <p>{{ $deliveryprice->city_id }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/deliveryprices.fields.price').':') !!}
    <p>{{ $deliveryprice->price }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/deliveryprices.fields.created_at').':') !!}
    <p>{{ $deliveryprice->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/deliveryprices.fields.updated_at').':') !!}
    <p>{{ $deliveryprice->updated_at }}</p>
</div>

