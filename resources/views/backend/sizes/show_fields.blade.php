<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/sizes.fields.id').':') !!}
    <p>{{ $size->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/sizes.fields.name').':') !!}
    <p>{{ $size->name }}</p>
</div>

<!-- Size Field -->
<div class="form-group">
    {!! Form::label('size', __('models/sizes.fields.size').':') !!}
    <p>{{ $size->size }}</p>
</div>

<!-- Cities Field -->
<div class="form-group">
    {!! Form::label('city_id', __('models/cities.plural').':') !!}
@empty($size->cities->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
    </p>
@else
    <p>
        @foreach ($size->cities as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Items Field -->
<div class="form-group">
    {!! Form::label('item_id', __('models/items.plural').':') !!}
@empty($size->items->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
    </p>
@else
    <p>
        @foreach ($size->items as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/sizes.fields.price').':') !!}
    <p>{{ $size->price }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/sizes.fields.created_at').':') !!}
    <p>{{ $size->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/sizes.fields.updated_at').':') !!}
    <p>{{ $size->updated_at }}</p>
</div>

