<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/sizes.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', __('models/sizes.fields.size').':') !!}
    {!! Form::text('size', null, ['class' => 'form-control']) !!}
</div>

<!-- Cities Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cities', __('models/cities.plural').':') !!}
    {!! Form::select('cities[]', $cityItems, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- Items Field -->
<div class="form-group col-sm-6">
    {!! Form::label('items', __('models/items.plural').':') !!}
    {!! Form::select('items[]', $itemItems, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/sizes.fields.price').':') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.sizes.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    })
</script>
@endpush
