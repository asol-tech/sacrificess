<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/cities.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-md-6">
    <label for="input-file-now-custom-1">{{ __('models/cities.fields.image').':' }}</label>
    <input type="file" name="image" id="input-file-now-custom-1" class="dropify" data-default-file="{{ $city->image ?? old('image') }}" />
</div>
<div class="clearfix"></div>

<!-- 'bootstrap / Toggle Switch Statu Field' -->
<div class="form-group col-sm-6">
    {!! Form::label('statu', __('models/cities.fields.statu').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('statu', 0) !!}
        {!! Form::checkbox('statu', 1, null,  ['data-toggle' => 'toggle']) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.cities.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            // Basic Dropify
            $('.dropify').dropify()
        });
    })
</script>
@endpush
