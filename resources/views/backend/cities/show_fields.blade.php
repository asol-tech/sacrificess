<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/cities.fields.id').':') !!}
    <p>{{ $city->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/cities.fields.name').':') !!}
    <p>{{ $city->name }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/cities.fields.image').':') !!}
    <p>{{ $city->image }}</p>
</div>

<!-- Statu Field -->
<div class="form-group">
    {!! Form::label('statu', __('models/cities.fields.statu').':') !!}
    <p>{{ $city->statu }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/cities.fields.created_at').':') !!}
    <p>{{ $city->created_at }}</p>
</div>

