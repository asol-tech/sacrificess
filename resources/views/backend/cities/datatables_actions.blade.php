{!! Form::open(['route' => ['backend.cities.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('backend.cities.show', $id) }}" class='btn btn-outline-primary btn-sm'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('backend.cities.edit', $id) }}" class='btn btn-outline-info btn-sm'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-sm sa-btn-delete',
        'data-item' => $name,
    ]) !!}
</div>
{!! Form::close() !!}
