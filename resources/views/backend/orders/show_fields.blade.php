<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/orders.fields.id').':') !!}
    <p>{{ $order->id }}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', __('models/orders.fields.client_id').':') !!}
    <p>{{ $order->client_id }}</p>
</div>

<!-- Staff Id Field -->
<div class="form-group">
    {!! Form::label('staff_id', __('models/orders.fields.staff_id').':') !!}
    <p>{{ $order->staff_id }}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', __('models/orders.fields.city_id').':') !!}
    <p>{{ $order->city_id }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', __('models/orders.fields.address').':') !!}
    <p>{{ $order->address }}</p>
</div>

<!-- Lat Field -->
<div class="form-group">
    {!! Form::label('lat', __('models/orders.fields.lat').':') !!}
    <p>{{ $order->lat }}</p>
</div>

<!-- Long Field -->
<div class="form-group">
    {!! Form::label('long', __('models/orders.fields.long').':') !!}
    <p>{{ $order->long }}</p>
</div>

<!-- Payment Field -->
<div class="form-group">
    {!! Form::label('payment', __('models/orders.fields.payment').':') !!}
    <p>{{ $order->payment }}</p>
</div>

<!-- Delivery Price Field -->
<div class="form-group">
    {!! Form::label('delivery_price', __('models/orders.fields.delivery_price').':') !!}
    <p>{{ $order->delivery_price }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/orders.fields.price').':') !!}
    <p>{{ $order->price }}</p>
</div>

<!-- Day Field -->
<div class="form-group">
    {!! Form::label('day', __('models/orders.fields.day').':') !!}
    <p>{{ $order->day }}</p>
</div>

<!-- Time Field -->
<div class="form-group">
    {!! Form::label('time', __('models/orders.fields.time').':') !!}
    <p>{{ $order->time }}</p>
</div>

<!-- Statu Field -->
<div class="form-group">
    {!! Form::label('statu', __('models/orders.fields.statu').':') !!}
    <p>{{ $order->statu }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/orders.fields.created_at').':') !!}
    <p>{{ $order->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/orders.fields.updated_at').':') !!}
    <p>{{ $order->updated_at }}</p>
</div>

