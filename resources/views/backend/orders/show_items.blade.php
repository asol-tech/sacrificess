
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">@lang('models/orderitems.plural') ({{ $order->orderitems->count() }})</h4>
            <h6 class="card-subtitle"></h6>
            <div id="accordion1" role="tablist" aria-multiselectable="true">
                @foreach ($order->orderitems as $orderitem)
                <div class="card m-b-0">
                    <div class="card-header" role="tab" id="headingOne1{{ $orderitem->id }}">
                        <h5 class="mb-0">
                        <a class="link collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1{{ $orderitem->id }}" aria-expanded="false" aria-controls="collapseOne">
                            {{ $orderitem->id }}
                        </a>
                        </h5>
                    </div>
                    <div id="collapseOne1{{ $orderitem->id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne1{{ $orderitem->id }}" style="">
                        <div class="card-body">
                            @include('backend.orderitems.show_fields')
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
