@extends('backend.layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">@lang('models/orders.singular')</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('backend.orders.index') }}">@lang('models/orders.singular')</a></li>
                    <li class="breadcrumb-item active">@lang('crud.detail')</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @include('backend.orders.show_fields')

                    @include('backend.orders.show_items')

                    <a href="{{ route('backend.orders.edit', $order->id) }}" class="btn btn-primary">
                        @lang('crud.edit')
                    </a>
                    <a href="{{ route('backend.orders.index') }}" class="btn btn-default">
                        @lang('crud.back')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
