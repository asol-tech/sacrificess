<!-- Client Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', __('models/orders.fields.client_id').':') !!}
    {!! Form::select('client_id', $userItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Staff Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('staff_id', __('models/orders.fields.staff_id').':') !!}
    {!! Form::select('staff_id', $userItems, null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', __('models/orders.fields.city_id').':') !!}
    {!! Form::select('city_id', $cityItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('address', __('models/orders.fields.address').':') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment', __('models/orders.fields.payment').':') !!}
    {!! Form::text('payment', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_price', __('models/orders.fields.delivery_price').':') !!}
    {!! Form::number('delivery_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day', __('models/orders.fields.day').':') !!}
    {!! Form::date('day', null, ['class' => 'form-control','id'=>'day']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#day').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time', __('models/orders.fields.time').':') !!}
    {!! Form::text('time', null, ['class' => 'form-control']) !!}
</div>

<!-- Statu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('statu', __('models/orders.fields.statu').':') !!}
    {!! Form::text('statu', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.orders.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
