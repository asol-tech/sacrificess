@empty($cities)
    <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
@else
    @foreach ($cities as $item)
        <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
    @endforeach
@endempty
