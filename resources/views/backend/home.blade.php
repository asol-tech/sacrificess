@extends('backend.layouts.app')

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">@lang('site.dashboard')</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">@lang('site.home')</a></li>
                <li class="breadcrumb-item active">@lang('site.dashboard')</li>
            </ol>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Info box -->
<!-- ============================================================== -->
<div class="card-group">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h3><i class="fa fa-users"></i></h3>
                            <p class="text-muted">@lang('site.total') @lang('models/users.plural')</p>
                        </div>
                        <div class="ml-auto">
                            <h2 class="counter text-primary">{{ $usersNum }}</h2>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h3><i class="fa fa-user"></i></h3>
                            <p class="text-muted">@lang('site.total') @lang('site.Moderators')</p>
                        </div>
                        <div class="ml-auto">
                            <h2 class="counter text-cyan">{{ $staffNum }}</h2>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-cyan" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h3><i class="fa fa-tags"></i></h3>
                            <p class="text-muted">@lang('site.total') @lang('site.Items')</p>
                        </div>
                        <div class="ml-auto">
                            <h2 class="counter text-purple">{{ $itemsNum }}</h2>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-purple" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h3><i class="fa fa-credit-card"></i></h3>
                            <p class="text-muted">@lang('site.total') @lang('site.Orders')</p>
                        </div>
                        <div class="ml-auto">
                            <h2 class="counter text-success">{{ $ordersNum }}</h2>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Info box -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">@lang('site.last') @lang('models/orders.plural')</h5>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>@lang('models/orders.fields.id')</th>
                            <th>@lang('models/orders.fields.client_id')</th>
                            <th>@lang('models/orders.fields.city_id')</th>
                            <th>@lang('models/orders.fields.created_at')</th>
                            <th>@lang('models/orders.fields.price')</th>
                            <th>@lang('models/orders.fields.statu')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td><a href="{{ route('backend.orders.edit', $order->id) }}" class="link"> #{{ $order->id }}</a></td>
                                <td>{{ $order->client->phone }}</td>
                                <td>{{ $order->city->name }}</td>
                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $order->created_at->diffForHumans() }}</span></td>
                                <td>${{ $order->price }}</td>
                                <td class="text-center">
                                    {{-- <div class="label label-table label-success"> --}}
                                        {{ $order->statu }}
                                    {{-- </div> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>

@endsection
