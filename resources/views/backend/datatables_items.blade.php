@empty($items)
    <span class="btn btn-xs fc btn-danger">@lang('site.NoItem')</span>
@else
    @foreach ($items as $item)
        <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
    @endforeach
@endempty
