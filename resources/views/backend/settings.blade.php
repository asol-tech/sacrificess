@extends('backend.layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">@lang('site.settings')</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                    <li class="breadcrumb-item active">@lang('site.settings')</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @include('flash::message')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    {!! Form::open(['route' => 'backend.settings']) !!}

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-user"></i></span>
                                    <span class="hidden-xs-down">@lang('site.setting_fields.social_login')</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-home"></i></span>
                                    <span class="hidden-xs-down">@lang('site.setting_fields.social_links')</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#mail" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span>
                                    <span class="hidden-xs-down">@lang('site.setting_fields.mail_settings')</span></a>
                            </li>
                        </ul>

                        <div class="tab-content tabcontent-border">
                            <div class="tab-pane active show" id="home" role="tabpanel">
                                <div class="p-20">
                                    <h4 class="card-title">@lang('site.setting_fields.social_login')</h4>
                                    @php
                                        $social_sites = ['facebook', 'google'];
                                    @endphp

                                    @foreach ($social_sites as $social_site)

                                        <!-- {{ $social_site }} client id Field -->
                                        <div class="form-group col-sm-6">
                                            {!! Form::label($social_site . '_client_id', __('site.setting_fields.' . $social_site . '_client_id').':') !!}
                                            {!! Form::text($social_site . '_client_id', settings()->get($social_site . '_client_id'), ['class' => 'form-control']) !!}
                                        </div>

                                        <!-- {{ $social_site }} client secret Field -->
                                        <div class="form-group col-sm-6">
                                            {!! Form::label($social_site . '_client_secret', __('site.setting_fields.' . $social_site . '_client_secret').':') !!}
                                            {!! Form::text($social_site . '_client_secret', settings()->get($social_site . '_client_secret'), ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- {{ $social_site }} redirect url Field -->
                                        <div class="form-group col-sm-6">
                                            {!! Form::label($social_site . '_redirect_url', __('site.setting_fields.' . $social_site . '_redirect_url').':') !!}
                                            {!! Form::text($social_site . '_redirect_url', settings()->get($social_site . '_redirect_url'), ['class' => 'form-control']) !!}
                                        </div>

                                    @endforeach

                                </div>
                            </div>

                            <div class="tab-pane" id="profile" role="tabpanel">
                                <div class="p-20">
                                    <h4 class="card-title">@lang('site.setting_fields.social_links')</h4>
                                    @php
                                        $social_sites = ['facebook', 'google', 'twitter', 'youtube'];
                                    @endphp

                                    @foreach ($social_sites as $social_site)

                                        <!-- {{ $social_site }} social link -->
                                        <div class="form-group col-sm-6">
                                            {!! Form::label($social_site . '_link', __('site.setting_fields.' . $social_site . '_link').':') !!}
                                            {!! Form::text($social_site . '_link', settings()->get($social_site . '_link'), ['class' => 'form-control']) !!}
                                        </div>

                                    @endforeach
                                </div>
                            </div>

                            <div class="tab-pane" id="mail" role="tabpanel">
                                <div class="p-20">
                                    <h4 class="card-title">@lang('site.setting_fields.mail_settings')</h4>

                                    <!-- mail from name -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_from_name', __('site.setting_fields.' . 'mail_from_name').':') !!}
                                        {!! Form::text('mail_from_name', settings()->get('mail_from_name'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail from address -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_from_address', __('site.setting_fields.' . 'mail_from_address').':') !!}
                                        {!! Form::text('mail_from_address', settings()->get('mail_from_address'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail driver -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_driver', __('site.setting_fields.' . 'mail_driver').':') !!}
                                        {!! Form::text('mail_driver', settings()->get('mail_driver'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail host -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_host', __('site.setting_fields.' . 'mail_host').':') !!}
                                        {!! Form::text('mail_host', settings()->get('mail_host'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail port -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_port', __('site.setting_fields.' . 'mail_port').':') !!}
                                        {!! Form::text('mail_port', settings()->get('mail_port'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail username -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_username', __('site.setting_fields.' . 'mail_username').':') !!}
                                        {!! Form::text('mail_username', settings()->get('mail_username'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail password -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_password', __('site.setting_fields.' . 'mail_password').':') !!}
                                        {!! Form::text('mail_password', settings()->get('mail_password'), ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- mail enc -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('mail_enc', __('site.setting_fields.' . 'mail_enc').':') !!}
                                        {!! Form::text('mail_enc', settings()->get('mail_enc'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
