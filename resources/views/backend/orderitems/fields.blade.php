<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', __('models/orderitems.fields.order_id').':') !!}
    {!! Form::select('order_id', $orderItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Item Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item', __('models/orderitems.fields.item').':') !!}
    {!! Form::select('item', $itemItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Size Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', __('models/orderitems.fields.size')) !!}
    {!! Form::select('size', $sizeItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', __('models/orderitems.fields.quantity').':') !!}
    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Choping Field -->
<div class="form-group col-sm-6">
    {!! Form::label('choping', __('models/orderitems.fields.choping').':') !!}
    {!! Form::select('choping', $chopingItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Cuting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cuting', __('models/orderitems.fields.cuting').':') !!}
    {!! Form::select('cuting', $cutingItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Packing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('packing', __('models/orderitems.fields.packing').':') !!}
    {!! Form::select('packing', $packingItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Head Field -->
<div class="form-group col-sm-6">
    {!! Form::label('head', __('models/orderitems.fields.head').':') !!}
    {!! Form::select('head', $headItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notes', __('models/orderitems.fields.notes').':') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.orderitems.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
