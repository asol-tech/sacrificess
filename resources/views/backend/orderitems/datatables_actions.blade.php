{!! Form::open(['route' => ['backend.orderitems.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('backend.orderitems.show', $id) }}" class='btn btn-outline-primary btn-sm'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('backend.orderitems.edit', $id) }}" class='btn btn-outline-info btn-sm'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-sm sa-btn-delete',
        'data-item' => $id,
    ]) !!}
</div>
{!! Form::close() !!}
