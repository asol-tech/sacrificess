<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/orderitems.fields.id').':') !!}
    <p>{{ $orderitem->id }}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', __('models/orderitems.fields.order_id').':') !!}
    <p>{{ $orderitem->order_id }}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', __('models/orderitems.fields.item').':') !!}
    <p>{{ $orderitem->item }}</p>
</div>

<!-- Size Id Field -->
<div class="form-group">
    {!! Form::label('size_id', __('models/orderitems.fields.size').':') !!}
    <p>{{ $orderitem->size }}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', __('models/orderitems.fields.quantity').':') !!}
    <p>{{ $orderitem->quantity }}</p>
</div>

<!-- Choping Field -->
<div class="form-group">
    {!! Form::label('choping', __('models/orderitems.fields.choping').':') !!}
    <p>{{ $orderitem->choping }}</p>
</div>

<!-- Cuting Field -->
<div class="form-group">
    {!! Form::label('cuting', __('models/orderitems.fields.cuting').':') !!}
    <p>{{ $orderitem->cuting }}</p>
</div>

<!-- Packing Field -->
<div class="form-group">
    {!! Form::label('packing', __('models/orderitems.fields.packing').':') !!}
    <p>{{ $orderitem->packing }}</p>
</div>

<!-- Head Field -->
<div class="form-group">
    {!! Form::label('head', __('models/orderitems.fields.head').':') !!}
    <p>{{ $orderitem->head }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', __('models/orderitems.fields.notes').':') !!}
    <p>{{ $orderitem->notes }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/orderitems.fields.price').':') !!}
    <p>{{ $orderitem->price }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/orderitems.fields.created_at').':') !!}
    <p>{{ $orderitem->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/orderitems.fields.updated_at').':') !!}
    <p>{{ $orderitem->updated_at }}</p>
</div>

