<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/roles.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('display_name', __('models/roles.fields.display_name').':') !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/roles.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Permissions -->
<div class="form-group">
    <h4>Permissions</h4>
    <table class="table table-hover">
        <thead>
        <tr>
            <th style="width: 5%;">#</th>
            <th style="width: 15%;">Model</th>
            <th>Permissions</th>
        </tr>
        </thead>

        <tbody>

        @php
            $models = ['roles', 'users', 'cities', 'items', 'sizes', 'additions', 'deliveryprices', 'orders', 'orderitems', 'settings', 'profile'];
            $permission_maps = ['create', 'read', 'update', 'delete'];
        @endphp

        @foreach ($models as $index=>$model)
            <tr>
                <td>{{ $index+1 }}</td>
                <td class="text-capitalize">{{ $model }}</td>
                <td>

                    @if (in_array($model, ['settings', 'profile']))
                        @php
                            $permission_maps = ['read', 'update'];
                        @endphp
                    @endif

                    <select name="permissions[]" class="form-control select2 no-validate" multiple>
                        @foreach ($permission_maps as $permission_map)
                            <option
                                value="{{ $permission_map . '_' . $model }}"
                                {{ !empty($role) && $role->hasPermission($permission_map . '_' . $model) ? 'selected' : '' }}
                            >{{ $permission_map }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.roles.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    })
</script>
@endpush
