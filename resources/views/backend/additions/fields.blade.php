<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/additions.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/additions.fields.price').':') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/additions.fields.type').':') !!}
    {!! Form::select('type',['choping' => __('models/additions.fields.typeOptions.choping'), 'cuting' => __('models/additions.fields.typeOptions.cuting'), 'packing' => __('models/additions.fields.typeOptions.packing'), 'head' => __('models/additions.fields.typeOptions.head')], null, ['class' => 'form-control']) !!}
</div>

<!-- Cities Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cities', __('models/cities.plural').':') !!}
    {!! Form::select('cities[]', $cityItems, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- Items Field -->
<div class="form-group col-sm-6">
    {!! Form::label('items', __('models/items.plural').':') !!}
    {!! Form::select('items[]', $itemItems, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- 'bootstrap / Toggle Switch Statu Field' -->
<div class="form-group col-sm-6">
    {!! Form::label('statu', __('models/additions.fields.statu').':') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('statu', 0) !!}
        {!! Form::checkbox('statu', 1, null,  ['data-toggle' => 'toggle']) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.additions.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    })
</script>
@endpush
