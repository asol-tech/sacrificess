<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/additions.fields.id').':') !!}
    <p>{{ $addition->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/additions.fields.name').':') !!}
    <p>{{ $addition->name }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', __('models/additions.fields.price').':') !!}
    <p>{{ $addition->price }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', __('models/additions.fields.type').':') !!}
    <p>{{ __('models/additions.fields.typeOptions.'.$addition->type) }}</p>
</div>

<!-- Cities Field -->
<div class="form-group">
    {!! Form::label('city_id', __('models/cities.plural').':') !!}
@empty($addition->cities->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
    </p>
@else
    <p>
        @foreach ($addition->cities as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Items Field -->
<div class="form-group">
    {!! Form::label('item_id', __('models/items.plural').':') !!}
@empty($addition->items->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
    </p>
@else
    <p>
        @foreach ($addition->items as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Statu Field -->
<div class="form-group">
    {!! Form::label('statu', __('models/additions.fields.statu').':') !!}
    <p>{{ $addition->statu }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/additions.fields.created_at').':') !!}
    <p>{{ $addition->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/additions.fields.updated_at').':') !!}
    <p>{{ $addition->updated_at }}</p>
</div>

