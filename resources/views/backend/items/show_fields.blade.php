<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/items.fields.id').':') !!}
    <p>{{ $item->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/items.fields.name').':') !!}
    <p>{{ $item->name }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/items.fields.image').':') !!}
    <div>
        <img
        src="{{ $item->image }}"
        alt="{{ $item->image }}"
        width="500px"
        srcset="{{ $item->image }}"
        class="img-thumbnail image-preview"
        >
    </div>
</div>

<!-- Cities Field -->
<div class="form-group">
    {!! Form::label('cities', __('models/cities.plural').':') !!}
@empty($item->cities->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoCity')</span>
    </p>
@else
    <p>
        @foreach ($item->cities as $itemy)
            <span class="btn btn-xs fc btn-primary">{{ $itemy['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Statu Field -->
<div class="form-group">
    {!! Form::label('statu', __('models/items.fields.statu').':') !!}
    <p>{{ $item->statu }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/items.fields.created_at').':') !!}
    <p>{{ $item->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/items.fields.updated_at').':') !!}
    <p>{{ $item->updated_at }}</p>
</div>

