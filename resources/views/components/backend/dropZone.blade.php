{{-- {!! Form::open(['route' => 'backend.mediaTmp', 'files' => true]) !!} --}}

    {{-- Name/Description fields, irrelevant for this article --}}
    <div class="form-group col-sm-12">
        <label for="document">@lang('site.gallery')</label>
        <div class="needsclick dropzone" id="document-dropzone">
        </div>
    </div>
{{-- {!! Form::close() !!} --}}

@push('scripts')
<script>
  var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '{{ route('backend.mediaTmp') }}',
    maxFilesize: 3, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="gallery[]"][value="' + name + '"]').remove()
    },
    init: function () {
      let myDropzone = this;
      @if(isset($items) && $items)
        var files =
          {!! json_encode($items) !!}
        for (var i in files) {
          var file = files[i]
          this.options.addedfile.call(this, file)
          this.options.thumbnail.call(this, file, file.getFullUrl)
          file.previewElement.classList.add('dz-complete')
          $('form').append('<input type="hidden" name="gallery[]" value="' + file.file_name + '">')
        }
      @endif
    }
  }
</script>
@endpush
