<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend-files') }}/assets/images/favicon.png">
    <title>@lang('auth.sign_in') - {{ config('app.name') }}</title>

    <!-- page css -->
    <link href="{{ asset('backend-files') }}/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend-files') }}/dist/css/style.min.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">{{ config('app.name') }} admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('front') }}/images/uploads/error-bg.jpg);background-position: bottom;">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" method="post" action="{{ url('/password/reset') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <h3 class="box-title m-b-20">@lang('auth.reset_password.title')</h3>
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input required="" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')">
                            </div>
                                @if ($errors->has('email'))
                                    <span class="form-control-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input required="" type="password" class="form-control" placeholder="@lang('auth.password')" name="password">
                            </div>
                            @if ($errors->has('password'))
                                <span class="form-control-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input required="" type="password" class="form-control" placeholder="@lang('auth.password_confirmation')" name="password_confirmation">
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="form-control-feedback">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">
                                    <i class="fa fa-btn fa-refresh"></i>@lang('auth.reset_password.reset_pwd_btn')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/popper/popper.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

</body>

</html>
