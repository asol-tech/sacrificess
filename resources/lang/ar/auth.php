<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',

    'full_name'        => 'الاسم الكامل',
    'email'            => 'Email',
    'password'         => 'Password',
    'confirm_password' => 'Confirm Password',
    'remember_me'      => 'Remember Me',
    'sign_in'          => 'Sign In',
    'or_via_social'    => 'Or via social',
    'sign_out'         => 'تسجيل الخروج',
    'register'         => 'Register',
    'verified'         => 'تم تفعيل رقم الهاتف الخاص بك',
    'verify_code_false'=> 'خطأ بكود التفيعل التفعيل',

    'login' => [
        'title'               => 'Sign in to start your session',
        'forgot_password'     => 'I forgot my password',
        'register_membership' => 'Register a new membership',
        'not_registerd'       => 'Don\'t have an account?',
    ],

    'registration' => [
        'title'                => 'Register a new membership',
        'create_account_enjoy' => 'Create your account and enjoy',
        'i_agree'              => 'I agree to',
        'terms'                => 'the terms',
        'have_membership'      => 'I already have a membership',
    ],

    'forgot_password' => [
        'title'           => 'Enter Email to reset password',
        'forgot_pwd_desc' => 'Enter your Email and instructions will be sent to you!',
        'send_pwd_reset'  => 'Send Password Reset Link',
        'back_to_login'   => 'Back to sign in',
    ],

    'reset_password' => [
        'title'         => 'Reset your password',
        'reset_pwd_btn' => 'Reset Password',
    ],

    'emails' => [
        'password' => [
            'reset_link' => 'Click here to reset your password',
        ],
    ],

    'app' => [
        'member_since' => 'Member since',
        'messages'     => 'Messages',
        'settings'     => 'Settings',
        'lock_account' => 'Lock Account',
        'profile'      => 'Profile',
        'online'       => 'Online',
        'search'       => 'Search',
        'create'       => 'Create',
        'export'       => 'Export',
        'print'        => 'Print',
        'reset'        => 'Reset',
        'reload'       => 'Reload',
    ],
];
