<?php

return [

    'record'      => 'عنصر',
    'add_new'      => 'اضف جديد',
    'cancel'       => 'الغاء',
    'cancelled'    => 'تم الالغاء!',
    'save'         => 'حفظ',
    'delete'       => 'مسح',
    'deleted'      => 'تم المسح!',
    'remove'       => 'ازالة',
    'edit'         => 'تعديل',
    'detail'       => 'تفاصيل',
    'back'         => 'رجوع',
    'action'       => 'اجراء',
    'id'           => 'رقم تعريفي',
    'created_at'   => 'تم الانشاء',
    'updated_at'   => 'تم التعديل',
    'deleted_at'   => 'مسح فى',
    'are_you_sure' => 'هل انت متاكد?',
    'you_wont_recover' => 'You will not be able to recover this imaginary file!',
    'you_will_delete'  => 'سوف تقوم بالمسح',
    'yes_delete'  => 'نعم, امسح!',
    'no_delete'  => 'ﻻ, الغاء!',
    'record_deleted'  => 'تم المسح.',
    'record_safe'  => 'لم يتم المسح :)',
];
