<?php

return array (
  'singular' => 'عنصر طلب',
  'plural' => 'عناصر الطلب',
  'fields' =>
  array (
    'id' => 'الرقم التعريفي',
    'order_id' => 'رقم الطلب',
    'item' => 'الذبيحة',
    'size' => 'الحجم',
    'quantity' => 'الكمية',
    'choping' => 'الفرم',
    'cuting' => 'التقطيع',
    'packing' => 'التغليف',
    'head' => 'الرأس',
    'notes' => 'ملاحظات',
    'price' => 'السعر',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
