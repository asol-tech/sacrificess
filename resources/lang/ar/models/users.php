<?php

return array (
  'singular' => 'المستخدم',
  'plural' => 'المستخدمين',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'name' => 'الاسم',
    'email' => 'الايميل',
    'phone' => 'الهاتف',
    'email_verified_at' => 'تم تفعيل الميل',
    'phone_verified_at' => 'تم تفعيل الهاتف',
    'password' => 'كلمة السر',
    'password_confirmation' => 'تاكيد كلمة السر',
    'city_id' => 'المنطقة',
    'statu' => 'الحالة',
    'statuOptions' => array(
        'pending' => 'بانتظار الموافقة',
        'active' => 'مفعل',
        'disabled' => 'معطل'

    ),
    'remember_token' => 'توكن',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
