<?php

return array (
  'singular' => 'الوظيفة',
  'plural' => 'الوظائف',
  'p_singular' => 'الصلاحية',
  'p_plural' => 'الصلاحيات',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'name' => 'الاسم',
    'display_name' => 'الاسم المعروض',
    'description' => 'الوصف',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
