<?php

return array (
  'singular' => 'المنطقة',
  'plural' => 'مناطق',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'name' => 'الاسم',
    'image' => 'الصورة',
    'statu' => 'الحالة',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
