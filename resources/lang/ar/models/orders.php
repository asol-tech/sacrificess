<?php

return array (
  'singular' => 'الطلب',
  'plural' => 'الطلبات',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'client_id' => 'العميل',
    'staff_id' => 'الموظف',
    'item_id' => 'الذبيحة',
    'city_id' => 'المدينة',
    'size_id' => 'الحجم',
    'quantity' => 'الكمية',
    'choping' => 'الفرم',
    'cuting' => 'التقطيع',
    'packing' => 'التغليف',
    'head' => 'الراس',
    'notes' => 'ملاحظات',
    'address' => 'العنوان',
    'lat' => 'لات',
    'long' => 'لونج',
    'payment' => 'الدفع',
    'delivery_price' => 'سعر التوصيل',
    'price' => 'السعر',
    'day' => 'اليوم',
    'time' => 'الوقت',
    'statu' => 'الحالة',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
