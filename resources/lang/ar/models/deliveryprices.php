<?php

return array (
  'singular' => 'سعر التوصيل',
  'plural' => 'اسعار التوصيل',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'city_id' => 'المدينة رقم تعريفي',
    'price' => 'السعر',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
