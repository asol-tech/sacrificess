<?php

return array (
  'singular' => 'اضافة',
  'plural' => 'اضافات',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'name' => 'الاسم',
    'price' => 'السعر',
    'type' => 'النوع',
    'typeOptions' => array(
        'choping' => 'الفرم',
        'cuting' => 'التقطيع',
        'packing' => 'التغليف',
        'head' => 'الرأس',
    ),
    'item_id' => 'الذبيحة رقم تعريفي',
    'city_id' => 'المدينة رقم تعريفي',
    'statu' => 'الحالة',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
