<?php

return array (
  'singular' => 'الحجم',
  'plural' => 'الاحجام',
  'fields' =>
  array (
    'id' => 'رقم تعريفي',
    'name' => 'الاسم',
    'size' => 'الحجم',
    'item_id' => 'الذبيحة رقم تعريفي',
    'city_id' => 'المدينة رقم تعريفي',
    'price' => 'السعر',
    'created_at' => 'تم الانشاء',
    'updated_at' => 'تم التعديل',
  ),
);
