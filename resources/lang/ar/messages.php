<?php

return [

    'retrieved' => ':model استلم بنجاح.',
    'saved'     => ':model حفظ بنجاح.',
    'updated'   => ':model تم التعديل بنجاح.',
    'deleted'   => ':model تم الجذف.',
    'deleted_error'   => ':model ﻻ يمكن حذفه, تفقد العلاقات.',
    'not_found' => ':model غير موجود',

];
