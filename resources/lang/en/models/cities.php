<?php

return array (
  'singular' => 'City',
  'plural' => 'Cities',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'image' => 'Image',
    'statu' => 'Statu',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
