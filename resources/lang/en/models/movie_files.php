<?php

return array (
  'singular' => 'MovieFile',
  'plural' => 'MovieFiles',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'movie_id' => 'Movie Id',
    'movie' => 'Movie',
    'path' => 'Path',
    'percent' => 'Percent',
    'quality' => 'Quality',
    'notes' => 'Notes',
    'ep' => 'Ep',
    'se' => 'Se',
    'duration' => 'Duration',
    'views' => 'Views',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
