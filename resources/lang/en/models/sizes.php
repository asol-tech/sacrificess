<?php

return array (
  'singular' => 'Size',
  'plural' => 'Sizes',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'size' => 'Size',
    'item_id' => 'Item Id',
    'city_id' => 'City Id',
    'price' => 'Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
