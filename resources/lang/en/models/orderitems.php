<?php

return array (
  'singular' => 'Orderitem',
  'plural' => 'Orderitems',
  'fields' =>
  array (
    'id' => 'Id',
    'order_id' => 'Order Id',
    'item' => 'Item',
    'size' => 'Size',
    'quantity' => 'Quantity',
    'choping' => 'Choping',
    'cuting' => 'Cuting',
    'packing' => 'Packing',
    'head' => 'Head',
    'notes' => 'Notes',
    'price' => 'Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
