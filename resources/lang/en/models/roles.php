<?php

return array (
  'singular' => 'Role',
  'plural' => 'Roles',
  'p_singular' => 'Permission',
  'p_plural' => 'Permissions',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'display_name' => 'Display Name',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
