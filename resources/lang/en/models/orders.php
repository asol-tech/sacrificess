<?php

return array (
  'singular' => 'Order',
  'plural' => 'Orders',
  'fields' =>
  array (
    'id' => 'Id',
    'client_id' => 'Client Id',
    'staff_id' => 'Staff Id',
    'item_id' => 'Item Id',
    'city_id' => 'City Id',
    'size_id' => 'Size Id',
    'quantity' => 'Quantity',
    'choping' => 'Choping',
    'cuting' => 'Cuting',
    'packing' => 'Packing',
    'head' => 'Head',
    'notes' => 'Notes',
    'address' => 'Address',
    'lat' => 'Lat',
    'long' => 'Long',
    'payment' => 'Payment',
    'delivery_price' => 'Delivery Price',
    'price' => 'Price',
    'day' => 'Day',
    'time' => 'Time',
    'statu' => 'Statu',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
