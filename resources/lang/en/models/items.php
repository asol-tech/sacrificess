<?php

return array (
  'singular' => 'Item',
  'plural' => 'Items',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'image' => 'Image',
    'statu' => 'Statu',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
