<?php

return array (
  'singular' => 'User',
  'plural' => 'Users',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'email_verified_at' => 'Email Verified At',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'city_id' => 'City',
    'statu' => 'Statu',
    'statuOptions' => array(
        'pending' => 'pending',
        'active' => 'active',
        'disabled' => 'disabled'

    ),
    'remember_token' => 'Remember Token',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
