<?php

return array (
  'singular' => 'Deliveryprice',
  'plural' => 'Deliveryprices',
  'fields' => 
  array (
    'id' => 'Id',
    'city_id' => 'City Id',
    'price' => 'Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
