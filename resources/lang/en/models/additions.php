<?php

return array (
  'singular' => 'Addition',
  'plural' => 'Additions',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'price' => 'Price',
    'type' => 'Type',
    'typeOptions' => array(
        'choping' => 'choping',
        'cuting' => 'cuting',
        'packing' => 'packing',
        'head' => 'head',
    ),
    'item_id' => 'Item Id',
    'city_id' => 'City Id',
    'statu' => 'Statu',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
