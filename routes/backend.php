<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BackendController@index')->name('home');
// Route::post('/mediaTmp', 'BackendController@mediaTmp')->name('mediaTmp');

// Route::match(['get', 'post'], 'movies/{movie}/crew', 'MovieController@crew')->name('movies.crew');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::match(['get', 'post'], 'settings', 'BackendController@settings')->name('settings');



Route::resource('cities', 'CityController');

Route::resource('items', 'ItemController');

Route::resource('sizes', 'SizeController');

Route::resource('additions', 'AdditionController');

Route::resource('deliveryprices', 'DeliverypriceController');

Route::resource('orders', 'OrderController');

Route::resource('orderitems', 'OrderitemController');
