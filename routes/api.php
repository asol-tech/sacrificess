<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'app'], function () {

    Route::resource('users', 'UserAPIController')->only('show', 'update')->middleware('auth:api');

    Route::post('users/login', 'UserAPIController@login')->name('api_login');
    Route::post('users/register', 'UserAPIController@register')->name('api_register');

    Route::post('users/user_auth', 'UserAPIController@user_auth')->name('api_user_auth');

    // Route::group([
    //     'namespace' => 'Auth',
    //     'middleware' => 'api',
    //     'prefix' => 'password'
    // ], function () {
    //     Route::post('resetlink', 'PasswordResetController@sendResetLinkEmail');
    //     Route::get('find/{token}', 'PasswordResetController@find');
    //     Route::post('reset', 'PasswordResetController@reset');
    // });

    Route::post('users/verify_code', 'UserAPIController@verify_code')->name('api_verify_code');

    Route::resource('cities', 'CityAPIController')->only('index', 'show');

    Route::resource('items', 'ItemAPIController')->only('index', 'show');

    // Route::resource('sizes', 'SizeAPIController');

    // Route::resource('additions', 'AdditionAPIController');

    Route::resource('deliveryprices', 'DeliverypriceAPIController')->only('index');

    Route::resource('orders', 'OrderAPIController')->middleware('auth:api')->only('index', 'show', 'store');

    // Route::resource('orderitems', 'OrderitemAPIController');

});

Route::group(['prefix' => 'mowasalen'], function () {


    Route::post('users/login', 'MowasalenAPIController@login_mowasalen')->name('api_login');
    // Route::post('users/register', 'MowasalenAPIController@register')->name('api_register');

    Route::post('users/verify_code', 'MowasalenAPIController@verify_code')->name('api_verify_code');

    // Route::group([
    //     'namespace' => 'Auth',
    //     'middleware' => 'api',
    //     'prefix' => 'password'
    // ], function () {
    //     Route::post('resetlink', 'PasswordResetController@sendResetLinkEmail');
    //     Route::get('find/{token}', 'PasswordResetController@find');
    //     Route::post('reset', 'PasswordResetController@reset');
    // });

    Route::group([
        'middleware' => 'auth:api',
    ], function () {

        Route::resource('users', 'MowasalenAPIController')->only('show', 'update');

        Route::post('users/logout', 'MowasalenAPIController@logout')->name('api_logout');

        Route::get('newOrders', 'MowasalenAPIController@newOrders');

        Route::get('orders', 'MowasalenAPIController@orders');

        Route::post('acceptOrders', 'MowasalenAPIController@acceptOrders');

        Route::post('deliverOrders', 'MowasalenAPIController@deliverOrders');

        Route::get('notifications', 'MowasalenAPIController@notifications');

        Route::get('notifications/read/{notification}', 'MowasalenAPIController@notificationsRead');

        // Route::resource('orders', 'OrderAPIController')->middleware('auth:api')->only('index', 'show', 'store');

    });


});

