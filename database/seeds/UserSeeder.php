<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'HaMaDa',
            'email' => 'admin@admin.com',
            'password' => 'password',
            'phone' => '11111111111',
            'statu' => 1,
        ]);
        $user->attachRole('super_admin');
    }
}
