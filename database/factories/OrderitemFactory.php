<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Orderitem;
use Faker\Generator as Faker;

$factory->define(Orderitem::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'item' => $faker->word,
        'size' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'choping' => $faker->word,
        'cuting' => $faker->word,
        'packing' => $faker->word,
        'head' => $faker->word,
        'notes' => $faker->text,
        'price' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
