<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Addition;
use Faker\Generator as Faker;

$factory->define(Addition::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'price' => $faker->word,
        'type' => $faker->word,
        'statu' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
