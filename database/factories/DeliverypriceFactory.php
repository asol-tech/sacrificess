<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Deliveryprice;
use Faker\Generator as Faker;

$factory->define(Deliveryprice::class, function (Faker $faker) {

    return [
        'city_id' => $faker->word,
        'price' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
