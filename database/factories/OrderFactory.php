<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'client_id' => $faker->word,
        'staff_id' => $faker->word,
        'city_id' => $faker->word,
        'address' => $faker->text,
        'lat' => $faker->randomDigitNotNull,
        'long' => $faker->randomDigitNotNull,
        'payment' => $faker->word,
        'delivery_price' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'day' => $faker->word,
        'time' => $faker->date('Y-m-d H:i:s'),
        'statu' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
