<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('staff_id')->nullable()->unsigned();
            $table->bigInteger('city_id')->unsigned();
            $table->text('notes')->nullable();
            $table->text('address');
            $table->string('lat');
            $table->string('long');
            $table->string('payment');
            $table->double('delivery_price');
            $table->double('price')->nullable();
            $table->date('day')->nullable();
            $table->time('time')->nullable();
            $table->boolean('statu')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('staff_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
