<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addition_item', function (Blueprint $table) {
            $table->bigInteger('addition_id')->unsigned();
            $table->bigInteger('item_id')->unsigned();
            $table->timestamps();
            $table->unique(['addition_id', 'item_id']);
            $table->foreign('addition_id')->references('id')->on('additions')->cascadeOnDelete();
            $table->foreign('item_id')->references('id')->on('items')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addition_item');
    }
}
