<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderitemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderitems', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('order_id')->unsigned();
            $table->string('item')->unsigned();
            $table->string('size')->unsigned();
            $table->integer('quantity');
            $table->string('choping')->nullable();
            $table->string('cuting')->nullable();
            $table->string('packing')->nullable();
            $table->string('head')->nullable();
            $table->text('notes')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('order_id')->references('id')->on('orders');
            // $table->foreign('item_id')->references('id')->on('items');
            // $table->foreign('size_id')->references('id')->on('sizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orderitems');
    }
}
