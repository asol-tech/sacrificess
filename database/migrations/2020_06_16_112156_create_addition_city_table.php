<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addition_city', function (Blueprint $table) {
            $table->bigInteger('addition_id')->unsigned();
            $table->bigInteger('city_id')->unsigned();
            $table->timestamps();
            $table->unique(['addition_id', 'city_id']);
            $table->foreign('addition_id')->references('id')->on('additions')->cascadeOnDelete();
            $table->foreign('city_id')->references('id')->on('cities')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addition_city');
    }
}
