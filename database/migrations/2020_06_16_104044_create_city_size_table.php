<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitySizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_size', function (Blueprint $table) {
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('size_id')->unsigned();
            $table->timestamps();
            $table->unique(['city_id', 'size_id']);
            $table->foreign('city_id')->references('id')->on('cities')->cascadeOnDelete();
            $table->foreign('size_id')->references('id')->on('sizes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_size');
    }
}
