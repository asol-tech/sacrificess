<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Orderitem;

class OrderitemApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_orderitem()
    {
        $orderitem = factory(Orderitem::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/backend/orderitems', $orderitem
        );

        $this->assertApiResponse($orderitem);
    }

    /**
     * @test
     */
    public function test_read_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/backend/orderitems/'.$orderitem->id
        );

        $this->assertApiResponse($orderitem->toArray());
    }

    /**
     * @test
     */
    public function test_update_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();
        $editedOrderitem = factory(Orderitem::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/backend/orderitems/'.$orderitem->id,
            $editedOrderitem
        );

        $this->assertApiResponse($editedOrderitem);
    }

    /**
     * @test
     */
    public function test_delete_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/backend/orderitems/'.$orderitem->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/backend/orderitems/'.$orderitem->id
        );

        $this->response->assertStatus(404);
    }
}
