<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Deliveryprice;

class DeliverypriceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/backend/deliveryprices', $deliveryprice
        );

        $this->assertApiResponse($deliveryprice);
    }

    /**
     * @test
     */
    public function test_read_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/backend/deliveryprices/'.$deliveryprice->id
        );

        $this->assertApiResponse($deliveryprice->toArray());
    }

    /**
     * @test
     */
    public function test_update_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();
        $editedDeliveryprice = factory(Deliveryprice::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/backend/deliveryprices/'.$deliveryprice->id,
            $editedDeliveryprice
        );

        $this->assertApiResponse($editedDeliveryprice);
    }

    /**
     * @test
     */
    public function test_delete_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/backend/deliveryprices/'.$deliveryprice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/backend/deliveryprices/'.$deliveryprice->id
        );

        $this->response->assertStatus(404);
    }
}
