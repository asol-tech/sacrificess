<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Addition;

class AdditionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_addition()
    {
        $addition = factory(Addition::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/backend/additions', $addition
        );

        $this->assertApiResponse($addition);
    }

    /**
     * @test
     */
    public function test_read_addition()
    {
        $addition = factory(Addition::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/backend/additions/'.$addition->id
        );

        $this->assertApiResponse($addition->toArray());
    }

    /**
     * @test
     */
    public function test_update_addition()
    {
        $addition = factory(Addition::class)->create();
        $editedAddition = factory(Addition::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/backend/additions/'.$addition->id,
            $editedAddition
        );

        $this->assertApiResponse($editedAddition);
    }

    /**
     * @test
     */
    public function test_delete_addition()
    {
        $addition = factory(Addition::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/backend/additions/'.$addition->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/backend/additions/'.$addition->id
        );

        $this->response->assertStatus(404);
    }
}
