<?php namespace Tests\Repositories;

use App\Models\Deliveryprice;
use App\Repositories\DeliverypriceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliverypriceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliverypriceRepository
     */
    protected $deliverypriceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliverypriceRepo = \App::make(DeliverypriceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->make()->toArray();

        $createdDeliveryprice = $this->deliverypriceRepo->create($deliveryprice);

        $createdDeliveryprice = $createdDeliveryprice->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryprice);
        $this->assertNotNull($createdDeliveryprice['id'], 'Created Deliveryprice must have id specified');
        $this->assertNotNull(Deliveryprice::find($createdDeliveryprice['id']), 'Deliveryprice with given id must be in DB');
        $this->assertModelData($deliveryprice, $createdDeliveryprice);
    }

    /**
     * @test read
     */
    public function test_read_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();

        $dbDeliveryprice = $this->deliverypriceRepo->find($deliveryprice->id);

        $dbDeliveryprice = $dbDeliveryprice->toArray();
        $this->assertModelData($deliveryprice->toArray(), $dbDeliveryprice);
    }

    /**
     * @test update
     */
    public function test_update_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();
        $fakeDeliveryprice = factory(Deliveryprice::class)->make()->toArray();

        $updatedDeliveryprice = $this->deliverypriceRepo->update($fakeDeliveryprice, $deliveryprice->id);

        $this->assertModelData($fakeDeliveryprice, $updatedDeliveryprice->toArray());
        $dbDeliveryprice = $this->deliverypriceRepo->find($deliveryprice->id);
        $this->assertModelData($fakeDeliveryprice, $dbDeliveryprice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_deliveryprice()
    {
        $deliveryprice = factory(Deliveryprice::class)->create();

        $resp = $this->deliverypriceRepo->delete($deliveryprice->id);

        $this->assertTrue($resp);
        $this->assertNull(Deliveryprice::find($deliveryprice->id), 'Deliveryprice should not exist in DB');
    }
}
