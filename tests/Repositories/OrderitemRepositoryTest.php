<?php namespace Tests\Repositories;

use App\Models\Orderitem;
use App\Repositories\OrderitemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderitemRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderitemRepository
     */
    protected $orderitemRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderitemRepo = \App::make(OrderitemRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_orderitem()
    {
        $orderitem = factory(Orderitem::class)->make()->toArray();

        $createdOrderitem = $this->orderitemRepo->create($orderitem);

        $createdOrderitem = $createdOrderitem->toArray();
        $this->assertArrayHasKey('id', $createdOrderitem);
        $this->assertNotNull($createdOrderitem['id'], 'Created Orderitem must have id specified');
        $this->assertNotNull(Orderitem::find($createdOrderitem['id']), 'Orderitem with given id must be in DB');
        $this->assertModelData($orderitem, $createdOrderitem);
    }

    /**
     * @test read
     */
    public function test_read_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();

        $dbOrderitem = $this->orderitemRepo->find($orderitem->id);

        $dbOrderitem = $dbOrderitem->toArray();
        $this->assertModelData($orderitem->toArray(), $dbOrderitem);
    }

    /**
     * @test update
     */
    public function test_update_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();
        $fakeOrderitem = factory(Orderitem::class)->make()->toArray();

        $updatedOrderitem = $this->orderitemRepo->update($fakeOrderitem, $orderitem->id);

        $this->assertModelData($fakeOrderitem, $updatedOrderitem->toArray());
        $dbOrderitem = $this->orderitemRepo->find($orderitem->id);
        $this->assertModelData($fakeOrderitem, $dbOrderitem->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_orderitem()
    {
        $orderitem = factory(Orderitem::class)->create();

        $resp = $this->orderitemRepo->delete($orderitem->id);

        $this->assertTrue($resp);
        $this->assertNull(Orderitem::find($orderitem->id), 'Orderitem should not exist in DB');
    }
}
