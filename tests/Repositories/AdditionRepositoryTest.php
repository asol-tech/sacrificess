<?php namespace Tests\Repositories;

use App\Models\Addition;
use App\Repositories\AdditionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AdditionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AdditionRepository
     */
    protected $additionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->additionRepo = \App::make(AdditionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_addition()
    {
        $addition = factory(Addition::class)->make()->toArray();

        $createdAddition = $this->additionRepo->create($addition);

        $createdAddition = $createdAddition->toArray();
        $this->assertArrayHasKey('id', $createdAddition);
        $this->assertNotNull($createdAddition['id'], 'Created Addition must have id specified');
        $this->assertNotNull(Addition::find($createdAddition['id']), 'Addition with given id must be in DB');
        $this->assertModelData($addition, $createdAddition);
    }

    /**
     * @test read
     */
    public function test_read_addition()
    {
        $addition = factory(Addition::class)->create();

        $dbAddition = $this->additionRepo->find($addition->id);

        $dbAddition = $dbAddition->toArray();
        $this->assertModelData($addition->toArray(), $dbAddition);
    }

    /**
     * @test update
     */
    public function test_update_addition()
    {
        $addition = factory(Addition::class)->create();
        $fakeAddition = factory(Addition::class)->make()->toArray();

        $updatedAddition = $this->additionRepo->update($fakeAddition, $addition->id);

        $this->assertModelData($fakeAddition, $updatedAddition->toArray());
        $dbAddition = $this->additionRepo->find($addition->id);
        $this->assertModelData($fakeAddition, $dbAddition->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_addition()
    {
        $addition = factory(Addition::class)->create();

        $resp = $this->additionRepo->delete($addition->id);

        $this->assertTrue($resp);
        $this->assertNull(Addition::find($addition->id), 'Addition should not exist in DB');
    }
}
