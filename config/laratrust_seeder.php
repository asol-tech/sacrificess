<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'super_admin' => [
            'roles' => 'c,r,u,d',
            'users' => 'c,r,u,d',
            'cities' => 'c,r,u,d',
            'items' => 'c,r,u,d',
            'sizes' => 'c,r,u,d',
            'additions' => 'c,r,u,d',
            'deliveryprices' => 'c,r,u,d',
            'orders' => 'c,r,u,d',
            'orderitems' => 'c,r,u,d',
            'settings' => 'r,u',
            'profile' => 'r,u'
        ],
        'staff' => [
            'profile' => 'r,u',
        ],
        'mowasalen' => [
            'profile' => 'r,u'
        ],
        'user' => [
            'profile' => 'r,u'
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
